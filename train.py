"""
Main callable script to train the neutrino regressor
"""
from pathlib import Path
from joblib import load

import torch as T
from torch.utils.data import DataLoader

from mattstools.trainer import Trainer
from mattstools.utils import save_yaml_files
from mattstools.torch_utils import train_valid_split

from nureg.network import SingleNeutrinoNSF, SingleNeutrinoFF
from nureg.datasets import NuRegData
from nureg.utils import get_nureg_configs

## For wandb bookeeping
import wandb

## Manual seed for reproducibility
T.manual_seed(42)


def main() -> None:
    """Run the script"""

    ## Collect the session arguments, returning the configuration dictionaries
    data_conf, net_conf, train_conf = get_nureg_configs()

    ## Start a weights and biases session
    wandb.init(
        entity="mleigh",
        project="NuFlows",
        name=net_conf.base_kwargs.name,
        resume=train_conf.trainer_kwargs.resume,
        id=train_conf.wandb_id or wandb.util.generate_id(),
        settings=wandb.Settings(_disable_stats=True),
    )
    train_conf.wandb_id = wandb.run.id

    ## Load the ttbar regression data and apply normalisation
    dataset = NuRegData(dset="train", **data_conf)

    ## Create the validation and training dataloaders
    train_set, valid_set = train_valid_split(dataset, train_conf.val_frac)
    train_load = DataLoader(train_set, **train_conf.loader_kwargs, shuffle=True)
    valid_load = DataLoader(valid_set, **train_conf.loader_kwargs)

    ## Get the data dimensions and add them the network configuraiton
    all_dims = dataset.get_dim()
    net_conf.base_kwargs.outp_dim = all_dims.pop(-1)  ## Neutrino dim is last
    net_conf.base_kwargs.inpt_dim = all_dims
    net_conf.prior_weight = 1 / len(train_set)

    ## Load the network
    if net_conf.base_kwargs.net_type == "flow":
        network = SingleNeutrinoNSF(**net_conf)
    else:
        network = SingleNeutrinoFF(**net_conf)
    print(network)

    ## Load and apply the saved scalers
    if train_conf.trainer_kwargs.resume:
        scalers = load(network.full_name / "scalers")
        dataset.apply_preprocess(scalers)

    ## Calculate new scalers and make plots before and after processing
    else:
        dataset.plot_variables(network.full_name / "train_dist")
        scalers = dataset.save_preprocess_info(network.full_name / "scalers")
        dataset.apply_preprocess(scalers)
        dataset.plot_variables(network.full_name / "train_dist")

    ## Create the save folder for the network and store the configuration dicts
    network.save_configs(data_conf, net_conf, train_conf)

    ## Load the trainer and run training
    trainer = Trainer(network, train_load, valid_load, **train_conf.trainer_kwargs)
    trainer.run_training_loop()


if __name__ == "__main__":
    main()
