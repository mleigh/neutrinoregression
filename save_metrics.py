from pathlib import Path
from joblib import load
from tqdm import tqdm
import numpy as np
import h5py

import torch as T
import torch.nn as nn

from mattstools.utils import load_yaml_files
from mattstools.torch_utils import sel_device, move_dev, to_np
from mattstools.bayesian import change_deterministic

from nureg.datasets import NuRegData
from nureg.physics import neut_to_pxpypz

T.set_grad_enabled(False)

from sklearn.mixture import GaussianMixture
from tqdm import tqdm
import matplotlib.pyplot as plt
from geomloss import SamplesLoss

def gmm_fits(inputs: np.ndarray, n_gaus: int = 2) -> None:
    """Runs a Gaussian Mixture model on each sample and returns the covariance"""
    metrics = []
    gmm_model = GaussianMixture(n_components = n_gaus)
    for sample in inputs:
        gmm = gmm_model.fit(sample)
        metrics.append(np.mean(np.abs(gmm.covariances_*gmm.weights_[:, None, None])))
    return np.vstack(metrics)


def get_gmm_metric(
    n_samples: int, network: nn.Module, data: list, batch_size: int = 256, n_gaus = 2
) -> tuple:

    # The gaussian mixture model
    gmm_model = GaussianMixture(n_components = n_gaus)
    all_values = []

    # For cycling through the batch of data
    n_events = len(data[0])
    n_batches = int(np.ceil(n_events / batch_size))
    for b in tqdm(range(n_batches)):

        # Get the network outputs
        b_str = b * batch_size
        b_end = min(b_str + batch_size, n_events)
        batch = list(d[b_str:b_end] for d in data)
        outputs = network.generate(batch, n_points=n_samples)
        outputs = to_np(outputs)

        # Each sample must be fit individually
        for sample in outputs:
            gmm = gmm_model.fit(sample)
            all_values.append(
                np.mean(np.abs(gmm.covariances_*gmm.weights_[:, None, None]))
            )

    return np.vstack(all_values)

def get_mmd_metric(
    n_samples: int, network: nn.Module, data: list, batch_size: int = 256, n_nets = 16,
) -> tuple:

    # The loss function to apply
    loss_fn = SamplesLoss("energy")
    all_dists = []

    # For cycling through the batch of data
    n_events = len(data[0])
    n_batches = int(np.ceil(n_events / batch_size))
    for b in tqdm(range(n_batches)):

        # Limits of the batch
        b_str = b * batch_size
        b_end = min(b_str + batch_size, n_events)
        batch = list(d[b_str:b_end] for d in data)

        # Get the nominal outputs
        change_deterministic(network, True)
        nominal_outputs = network.generate(batch, n_points=n_samples)

        # Cycle for each stocastic sampling
        dists = []
        change_deterministic(network, False)
        for n in range(n_nets):
            outputs = network.generate(batch, n_points=n_samples)
            dists.append(loss_fn(nominal_outputs, outputs))
        all_dists.append(T.stack(dists).mean(dim=0))

    return to_np(T.cat(all_dists))

def save_network_metrics(
    model_dir: Path,
    model_name: str,
    flag: str,
    metric: str,
    data_folder: Path,
    output_folder: Path,
    n_samples: int = 128,
) -> None:
    """Creates a HDF file containing network outputs and likelihoods given
    a model and some target files
    """

    ## Load the network
    device = sel_device("cuda")
    network = T.load(model_dir / model_name / "best", map_location=device)
    network.device = device
    network.eval()

    ## Load the data using the network's specific config file
    data_conf = load_yaml_files(model_dir / model_name / "config/data.yaml")
    data_conf["path"] = data_folder
    del data_conf["variables"]["neutrinos"] # delete the truth information, not needed
    dataset = NuRegData("test", **data_conf)

    ## Load and apply the scalers, then move to the device
    scalers = load(model_dir / model_name / "scalers")
    dataset.apply_preprocess(scalers)
    data = move_dev(dataset[:], device)

    ## Sample from the network multiple times then undo scaling
    if metric == "gmm":
        metrics = get_gmm_metric(n_samples, network, data)
    if metric == "mmd":
        metrics = get_mmd_metric(n_samples, network, data)

    ## Save the outputs as a table
    with h5py.File(output_folder / f"{model_name}_{flag}_{metric}", "w") as hf:
        hf.create_dataset("metrics", data=metrics)

def main() -> None:

    ## Create a list of network paths to load and compare
    network_folder = Path(
        "/home/users/l/leighm/scratch/Saved_Networks/NuFlowsRedux/BayesianSearch"
    )
    model_names = [
        ("60399548_35", "NuFlow", "gmm"),
        ("60399548_56", "BNuFlow", "mmd"),
    ]

    ## Save the network outputs to an HDF file
    for mn, flg, metric in model_names:
        print(mn)

        save_network_metrics(
            model_dir=network_folder,
            model_name=mn,
            flag=flg,
            metric=metric,
            data_folder=Path(
                "/srv/beegfs/scratch/groups/rodem/ttbar_evt_reco/data/new_channels/"
            ),
            output_folder=Path("/home/users/l/leighm/scratch/NuFlowOutputs/semilep_ttbar/"),
            n_samples=1024,
        )


if __name__ == "__main__":
    main()
