import time
import torch as T
from pathlib import Path
from joblib import load

from mattstools.utils import load_yaml_files
from mattstools.torch_utils import sel_device, move_dev, to_np
from mattstools.bayesian import change_deterministic

from nureg.datasets import NuRegData


@T.no_grad()
def main():

    ###########################
    model_dir = Path(
        "/home/users/l/leighm/scratch/Saved_Networks/NuFlowsRedux/BayesianSearch"
    )
    flow_name = "60399548_35"
    num_samples = 100
    ###########################

    ## Load the networks
    device = sel_device("cpu")
    flow = T.load(model_dir / flow_name / "best", map_location=device)
    flow.device = device
    flow.eval()

    ## Load the data using the flow's config file
    data_conf = load_yaml_files(model_dir / flow_name / "config/data.yaml")
    dataset = NuRegData("test", **data_conf)
    scalers = load(model_dir / flow_name / "scalers")
    dataset.apply_preprocess(scalers)

    ## Loop through the samples
    for i in range(100):

        sample = move_dev(dataset[i : i + 1], device)

        ts = time.time()
        a = flow(sample)
        print(time.time() - ts)

        ts = time.time()
        flow.generate(sample, n_points=1)
        print(time.time() - ts)


if __name__ == "__main__":
    main()
