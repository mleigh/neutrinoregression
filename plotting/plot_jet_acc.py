from pathlib import Path

import h5py
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from mattstools.plotting import plot_multi_hists

from old_scripts.plot_hists_old import read_and_mask
from nureg.physics import Mom4Vec
from nureg.plotting import nu_plot_name, nu_plot_colour, nu_plot_ls


TOP_MASS = 172.76
W_MASS = 80.379


def wald_interval(ns, n, z=1.645):
    """Return the wilsons interval given the number of successes and total trials"""
    nf = n - ns
    p_hat = ns / n
    err = z / n * np.sqrt(ns * nf / n)
    return p_hat, err


def make_acuracy_plots(neutrino_list, data_path, max_jets, evt_sels):
    pad_len = max([len(nu) for nu in neutrino_list])

    ## For each event selection
    for evt_sel in evt_sels:

        ## Create the firgure now
        fig, ax = plt.subplots(4, 1, figsize=(4, 8))
        ax = ax.flatten()

        ## First we need to load the true data
        _, _, jets, _, n_jets, jet_idx, _, _ = read_and_mask(
            data_path, max_jets, evt_sel
        )

        ## For each neutrino
        for nu in neutrino_list:

            ## Initiliase the dataframe with the number of jets in the event
            df = pd.DataFrame({"n_jets": n_jets})

            ## Load the results
            with h5py.File(f"data/chi_squared_flex/{evt_sel}/{nu}", "r") as f:
                best_neut = f["best_neut"][:]
                jet_order = f["jet_orders"][:]

            ## Check the overall jet matching accuracy
            jet_corr = jet_idx == jet_order[:, :4]
            all_corr = np.sum(jet_corr, axis=-1) == 4
            jet_acc = np.mean(jet_corr, axis=0) * 100
            all_acc = np.mean(all_corr, axis=0) * 100

            ## Print the accuracies as percentages
            # print(f"{nu:{pad_len}}:", end="")
            for i, (nm, col) in enumerate(zip(["blep", "bhad", "q1", "q2"], jet_acc.T)):
                # print(f"{nm} = {col:5.2f}, ", end="")
                df[nm] = jet_corr[:, i]  ## Also save the correct pecentages
            # print(f"All = {all_acc:.2f}")

            ## Add total accuracy and group by the number of jets
            df["all_cor"] = all_corr
            n_events = df.groupby("n_jets").size().to_numpy()
            df = df.groupby("n_jets").sum()

            print(nu)
            print(
                "blep",
                np.around(
                    (df / np.expand_dims(n_events, -1))["blep"].to_numpy(), decimals=3
                ),
            )

            print(
                "bhad",
                np.around(
                    (df / np.expand_dims(n_events, -1))["bhad"].to_numpy(), decimals=3
                ),
            )

            print(
                "q1",
                np.around(
                    (df / np.expand_dims(n_events, -1))["q1"].to_numpy(), decimals=3
                ),
            )
            print()

            ## Add the plot to the figure
            for i, nm in enumerate(["blep", "bhad", "q1", "q2"]):
                p_hat, err = wald_interval(df[nm].to_numpy(), n_events)
                ax[i].errorbar(df.index.to_numpy(), p_hat, err, label=nu)

                ## Add labels and legends
                ax[i].axes.xaxis.set_visible(False)
                ax[i].set_ylabel(f"{nm} acc")

        ## Plotting folder
        plot_folder = Path(f"plots/chi_squared_flex/{evt_sel}")
        plot_folder.mkdir(parents=True, exist_ok=True)

        ax[0].legend()
        ax[-1].axes.xaxis.set_visible(True)
        ax[-1].set_xlabel("Number of jets")
        fig.tight_layout()
        fig.subplots_adjust(hspace=0)
        fig.savefig(plot_folder / "accuracy")
        plt.close(fig)


def make_mass_correct(neutrino_list, data_path, max_jets, evt_sels):

    ## For each event selection
    for evt_sel in evt_sels:
        print(evt_sel)

        ## First we need to load the true data
        met, leptons, jets, tru_nu, n_jets, jet_idx, njet_sel, sel = read_and_mask(
            data_path, max_jets, evt_sel
        )

        ## For each neutrino we make a plot
        plot_folder = Path(f"plots/chi_squared/{evt_sel}/match_mass")
        plot_folder.mkdir(parents=True, exist_ok=True)

        ## Get the truth mass plot for the blep je
        sample_idxs = np.arange(len(jets))
        tru_blep_idx = jet_idx[:, 0]
        if evt_sel != "none":
            tru_blv_mass = Mom4Vec(
                jets.mom[sample_idxs, tru_blep_idx]
                + leptons.mom
                + np.squeeze(tru_nu.mom)
            ).mass

        ## For neutrino we make a seperate plot
        for nu in neutrino_list:

            with h5py.File(f"data/chi_squared_flex/{evt_sel}/{nu}", "r") as f:
                best_neut = f["best_neut"][:]
                blep_idx = f["jet_orders"][:, 0]

            ## Get the masses of the chisq blep, and trim for only those correctly assigned
            blv_all = Mom4Vec(
                jets.mom[sample_idxs, blep_idx] + leptons.mom + best_neut
            ).mass
            blv_corr = blv_all[(blep_idx == tru_blep_idx) & (blep_idx != 9999)]

            ## The plotting name for the neutrino
            nu_name = nu_plot_name(nu)
            nu_col = nu_plot_colour(nu)

            hists = [blv_all, blv_corr]
            labels = [rf"{nu_name}, $\chi^2$", rf"{nu_name}, $\chi^2$ (Correct)"]
            hist_kwargs = {"linestyle": ["-", "-"], "zorder": [1, 2]}
            hist_colours = [nu_col, nu_col]
            hist_fills = [0, 1]

            if evt_sel != "none":
                hists.insert(0, tru_blv_mass)
                labels.insert(0, "Idealised")
                hist_kwargs["linestyle"].insert(0, "--")
                hist_kwargs["zorder"].insert(0, 1000)
                hist_colours.insert(0, "k")
                hist_fills.insert(0, 0)

            plot_multi_hists(
                plot_folder / f"{nu}_lnu",
                hists,
                labels,
                r"$m_{bl\nu}$ [GeV]",
                bins=np.linspace(110, 240, 30),
                ylim=[0, 0.27],
                hist_fills=hist_fills,
                hist_colours=hist_colours,
                hist_kwargs=hist_kwargs,
                hist_scale=1 / len(met),
                incl_overflow=True,
                incl_underflow=True,
                as_pdf=True,
            )


# def make_mass_plots(neutrino_list, data_path, max_jets, evt_sels, only_corr=True):

#     ## For each event selection
#     for evt_sel in evt_sels:
#         print(evt_sel)

#         ## The all jets selection can then also have the exact jet masses
#         if evt_sel in ["all_jets", "blep"]:
#             if "Idealised" not in neutrino_list:
#                 neutrino_list.append("Idealised")
#         else:
#             neutrino_list.remove("Idealised")

#         ## Initialise empty lists for the masses
#         qq_masses = []
#         bqq_masses = []
#         lv_masses = []
#         blv_masses = []

#         ## First we need to load the true data
#         met, leptons, jets, tru_nu, n_jets, jet_idx, njet_sel, sel = read_and_mask(
#             data_path, max_jets, evt_sel
#         )

#         ## For each neutrino
#         for nu in neutrino_list:

#             ## For the truth mass, select the real neutrino and the jet selection
#             if nu == "Idealised":
#                 best_neut = np.squeeze(tru_nu.mom)
#                 jet_order = jet_idx
#                 jet_order[jet_order == 9999] = -1

#             ## Otherwise load the results from the chi_squared fit
#             else:

#                 with h5py.File(f"data/chi_squared/{evt_sel}/{nu}", "r") as f:
#                     best_neut = f["best_neut"][:]
#                     jet_order = f["jet_orders"][:]

#             ## Calculate the masses of each event based on either true assoc or chi
#             sample_idxs = np.arange(len(jets))
#             blep = Mom4Vec(jets.mom[sample_idxs, jet_order[:, 0]])
#             bhad = Mom4Vec(jets.mom[sample_idxs, jet_order[:, 1]])
#             w_q1 = Mom4Vec(jets.mom[sample_idxs, jet_order[:, 2]])
#             w_q2 = Mom4Vec(jets.mom[sample_idxs, jet_order[:, 3]])

#             ## Calculate the kinematics of the parent particles
#             qq_obj = Mom4Vec(w_q1.mom + w_q2.mom)
#             bqq_obj = Mom4Vec(bhad.mom + qq_obj.mom)
#             lv_obj = Mom4Vec(leptons.mom + best_neut)
#             blv_obj = Mom4Vec(blep.mom + lv_obj.mom)

#             ## Store their masses
#             qq_masses.append(qq_obj.mass)
#             bqq_masses.append(bqq_obj.mass)
#             lv_masses.append(lv_obj.mass)
#             blv_masses.append(blv_obj.mass)

#             ## Only score the correctly associated masses
#             if only_corr:
#                 jet_corr = jet_idx == jet_order[:, :4]
#                 blep_corr = jet_corr[:, 0]
#                 bhad_corr = jet_corr[:, 1]
#                 qq_corr = jet_corr[:, 2] & jet_corr[:, 3]

#                 qq_masses[-1] = qq_masses[-1][qq_corr]
#                 bqq_masses[-1] = bqq_masses[-1][bhad_corr & qq_corr]
#                 blv_masses[-1] = blv_masses[-1][blep_corr]

#         ## Plot the distributions
#         plot_folder = Path(f"plots/chi_squared/{evt_sel}/")
#         if only_corr:
#             plot_folder /= "corr"
#         plot_folder.mkdir(parents=True, exist_ok=True)

#         plot_multi_hists(
#             plot_folder / "lnu",
#             lv_masses,
#             [nu_plot_name(nu) for nu in neutrino_list],
#             r"$m_{l\nu}$ [GeV]",
#             logy=True,
#             ylim=[1e-4, 1.2],
#             bins=np.linspace(40, 120, 55),
#             normed=False,
#             incl_overflow=True,
#             incl_underflow=True,
#             col_colours=[nu_plot_colour(nu) for nu in neutrino_list],
#             col_kwargs={"linestyle": [nu_plot_ls(nu) for nu in neutrino_list]},
#             div=1e5,
#         )

#         plot_multi_hists(
#             plot_folder / "blnu",
#             blv_masses,
#             [nu_plot_name(nu) for nu in neutrino_list],
#             r"$m_{bl\nu}$ [GeV]",
#             ylim=[0, 0.55 if evt_sel == "all_jets" else 1.1],
#             bins=np.linspace(110, 230, 50),
#             normed=False,
#             incl_overflow=True,
#             incl_underflow=True,
#             col_colours=[nu_plot_colour(nu) for nu in neutrino_list],
#             col_kwargs={"linestyle": [nu_plot_ls(nu) for nu in neutrino_list]},
#             div=1e4,
#         )

#         plot_multi_hists(
#             plot_folder / "qq",
#             qq_masses,
#             [nu_plot_name(nu) for nu in neutrino_list],
#             r"$m_{qq}$ [GeV]",
#             ylim=[0, 0.3],
#             bins=np.linspace(50, 110, 50),
#             normed=False,
#             incl_overflow=True,
#             incl_underflow=True,
#             col_colours=[nu_plot_colour(nu) for nu in neutrino_list],
#             col_kwargs={"linestyle": [nu_plot_ls(nu) for nu in neutrino_list]},
#             div=1e4,
#         )

#         plot_multi_hists(
#             plot_folder / "bqq",
#             bqq_masses,
#             [nu_plot_name(nu) for nu in neutrino_list],
#             r"$m_{qq}$ [GeV]",
#             ylim=[0, 0.4],
#             bins=np.linspace(100, 250, 50),
#             normed=False,
#             incl_overflow=True,
#             incl_underflow=True,
#             col_colours=[nu_plot_colour(nu) for nu in neutrino_list],
#             col_kwargs={"linestyle": [nu_plot_ls(nu) for nu in neutrino_list]},
#             div=1e4,
#         )


def main():

    ########################
    data_path = Path("data/events_0_selected.h5")
    max_jets = 10
    neutrino_list = [
        "Truth",
        "MET_Quad",
        "NuReg_FFNoQuad_1",
        "NuReg_FlowNoQuad_1",
        "NuReg_FlowNoQuad_Mode",
    ]
    evt_sels = ["all_jets"]
    ########################

    ## Make mass plots with and without the corr requrement
    # make_mass_plots(neutrino_list, data_path, max_jets, evt_sels, True)
    # make_mass_plots(neutrino_list, data_path, max_jets, evt_sels, False)

    ## Make mass correct plots
    # make_mass_correct(neutrino_list, data_path, max_jets, evt_sels)
    make_acuracy_plots(neutrino_list, data_path, max_jets, evt_sels)


if __name__ == "__main__":
    main()

## Blep
# 0.652 0.668 0.643 0.612 0.596 0.596
# 0.593 0.51  0.445 0.386 0.36  0.316
# 0.588 0.498 0.415 0.354 0.332 0.28
# 0.61  0.555 0.505 0.447 0.394 0.394
# 0.619 0.593 0.545 0.492 0.481 0.42

## None
# 0.555 0.59  0.579 0.553 0.548 0.54
# 0.505 0.451 0.4   0.349 0.331 0.286
# 0.501 0.44  0.374 0.32  0.305 0.254
# 0.519 0.491 0.454 0.404 0.362 0.357
# 0.527 0.524 0.49  0.445 0.442 0.38

## All
# 0.860 0.756 0.687 0.641 0.617 0.603
# 0.783 0.568 0.467 0.392 0.366 0.278
# 0.774 0.552 0.433 0.368 0.338 0.254
# 0.799 0.618 0.531 0.473 0.398 0.381
# 0.818 0.669 0.578 0.516 0.489 0.437
