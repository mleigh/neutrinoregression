from pathlib import Path
import h5py

from tqdm import tqdm
import numpy as np
from joblib import load
from dotmap import DotMap
import matplotlib.pyplot as plt

import torch as T
from torch.utils.data import DataLoader

from mattstools.utils import load_yaml_files
from mattstools.torch_utils import sel_device, move_dev, to_np
from mattstools.bayesian import change_deterministic

from nureg.network import SingleNeutrinoNSF
from nureg.datasets import NuRegData
from nureg.physics import neut_to_pxpypz, combine_comps
from nureg.plotting import nu_plot_colour

T.set_grad_enabled(False)


def main() -> None:

    ###########################

    ## The directories for loading the network/data and saving the plots
    model_dir = Path(
        "/home/users/l/leighm/scratch/Saved_Networks/NuFlowsRedux/BayesianSearch"
    )
    data_path = Path(
        "/srv/beegfs/scratch/groups/rodem/ttbar_evt_reco/data_share/20220204_ttbar_ljets_moreevents/"
    )
    plot_folder = Path("plots/Uncertainty/")

    ## Networks (file_name, plot_name, colour)
    flows = [
        ("60399548_35", r"$\nu$-Flows", "C1"),
        # ("60399548_56", "Bayesian Flow", "C3"),
    ]

    ## Specifiers for the how to generate samples
    is_bayesian = False
    batch_size = 1024
    num_samples = 64
    quantiles = np.linspace(0, 1, 10)
    dims = ["px", "py", "eta", "E"]

    ###########################

    ## Make the plotting folder
    plot_folder.mkdir(parents=True, exist_ok=True)

    ## Create the figures for each dimnsion
    figs, axes = [], []
    for d in dims:
        fig, ax = plt.subplots(figsize=(4, 4))
        ax.set_xlabel("Quantile q")
        ax.set_ylabel(r"Observed Pr\[$X \leq F^{-1}(q)$\]")
        ax.set_title(f"{d} marginal")
        ax.set_xlim([0, 1])
        ax.set_ylim([0, 1])
        ax.plot([0, 1], [0, 1], "k--")
        ax.grid()
        fig.set_tight_layout(True)
        figs.append(fig)
        axes.append(ax)

    ## Cycle through the flows
    for flow_name, plot_name, plot_colour in flows:

        ## Load the flow
        device = sel_device("cuda:5")
        # net_conf = DotMap(load_yaml_files(model_dir / flow_name / "config/net.yaml"))
        # flow = SingleNeutrinoNSF(**net_conf)
        flow = T.load(model_dir / flow_name / "best", map_location=device)
        flow.device = device
        flow.eval()

        ## Load the test data using the flow's config file
        data_conf = DotMap(load_yaml_files(model_dir / flow_name / "config/data.yaml"))
        data_conf.path = data_path
        dataset = NuRegData("test", **data_conf)

        ## Save the truth before the pre-processing
        truth_nu = dataset.data["neutrinos"].numpy()

        ## Load and apply the scalers and move to device
        scalers = load(model_dir / flow_name / "scalers")
        dataset.apply_preprocess(scalers)

        ## Create the dataloader to allow for batched passing
        loader = DataLoader(dataset, batch_size=batch_size)
        outputs = []
        for batch in tqdm(loader):
            batch = move_dev(batch, device)

            # modes = []
            # for i in range(num_samples):
            # modes.append(flow.get_mode(batch, n_points=num_samples).unsqueeze(1))
            # modes = T.cat(modes, dim=1)
            # outputs.append(modes)

            outputs.append(flow.generate(batch, n_points=num_samples))
        outputs = T.cat(outputs, dim=0).cpu()

        ## Applying the inverse transform of the scalers
        outputs = outputs.view(-1, outputs.shape[-1])
        outputs = T.from_numpy(scalers["neutrinos"].inverse_transform(outputs))
        outputs = outputs.view(-1, num_samples, outputs.shape[-1])

        ## Add an energy column to the neutrinos
        outputs = np.concatenate(
            [outputs, np.linalg.norm(outputs, axis=-1, keepdims=True)], axis=-1
        )
        truth_nu = np.concatenate(
            [truth_nu, np.linalg.norm(truth_nu, axis=-1, keepdims=True)], axis=-1
        )

        ## Calculate how many samples are greate than truth in each dimension
        for i, dim in enumerate(dims):
            quants = np.quantile(outputs[..., i], q=quantiles, axis=-1).T
            frac_greater = (quants > truth_nu[..., i, None]).mean(axis=0)

            ## Add the lines
            axes[i].plot(quantiles, frac_greater, plot_colour, label=plot_name)

    ## Plot and close
    for d, fig, ax in zip(dims, figs, axes):
        ax.legend()
        fig.savefig(plot_folder / f"{d}.png")
        plt.close(fig)


if __name__ == "__main__":
    main()
