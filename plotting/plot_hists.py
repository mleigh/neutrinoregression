"""
Script for plotting distributions from a single flow
"""


from collections import defaultdict
from cProfile import label
from itertools import cycle, permutations
from pathlib import Path
from typing import Any, List, Optional, Tuple

import h5py
import matplotlib.pyplot as plt
import numpy as np
import torch as T
from mattstools.plotting import plot_corr_heatmaps, plot_multi_hists
from nureg.io_utils import get_quality_mask, get_quality_metric, read_and_mask
from nureg.physics import Mom4Vec
from nureg.plotting import nu_plot_colour, nu_plot_ls, nu_plot_name
from tqdm import tqdm

TOP_MASS = 173
W_MASS = 80.379
QQ_SIGMA = 18.07
BQQ_SIGMA = 27.17

LNU_SIGMA = defaultdict(
    lambda: 24.84,
    {
        "Truth": 4.6781764,
        "MET_Quad": 31.117254,
        "NuReg_Large_1024": 5.6434116,
        "NuReg_Large_Mode": 1.287631,
    },
)

BLNU_SIGMA = defaultdict(
    lambda: 33.04,
    {
        "Truth": 17.334011,
        "MET_Quad": 50.925045,
        "NuReg_Large_1024": 33.672394,
        "NuReg_Large_Mode": 24.807196,
    },
)


T.set_grad_enabled(False)


def rms(arr: np.ndarray, axis: Optional[int] = None) -> np.ndarray:
    """Returns RMS of a numpy array along a dimension"""
    return np.sqrt(np.mean(arr * arr, axis=axis))


def rmse_vs_cut_plots(
    plot_dir: Path,
    file_path: Path,
    export_dir: str,
    neutrino_list: list,
    max_jets: int,
    evt_sel: str,
    metrics: List[Tuple[str, str]],
) -> None:
    """Create plots which show the RMSE for the top mass verses the cut efficiency"""

    ## Create the plotting folder
    plot_dir = plot_dir / f"{evt_sel}_{max_jets}/metrics/"
    plot_dir.mkdir(parents=True, exist_ok=True)

    ## Create the figure
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    linecycler = cycle(["-", "--", "-.", ":"])

    ## Load everything once at the start
    data, nu_data, mask = read_and_mask(file_path, export_dir, neutrino_list, max_jets, evt_sel)

    ## Plot the metric vs performance correlation plots
    for metric in metrics:

        # Load the quality metric from file
        with h5py.File(Path(export_dir, metric), "r") as f:
            qual = f["metrics"][:].flatten()[mask]

        # Get reasonable limits of the metric using the quantiles
        amin, amax = tuple(np.quantile(qual, [0.02, 0.98]))

        # Plot the correlation to the truth z
        plot_corr_heatmaps(
            data.tru_nu.pz.flatten(),
            qual,
            [np.linspace(-200, 200, 50), np.linspace(amin, amax, 50)],
            "Truth " + r"$p_z^{\nu}$ [GeV]",
            metric,
            plot_dir / f"{metric}_vs_pz",
            equal_aspect = False,
            incl_line = False,
            do_pdf = False,
        )

        # Plot the correlation to the truth mblv
        if evt_sel != "none":
            sample_idxs = np.arange(len(data.jets))
            blep = data.jets[sample_idxs, data.jet_idx[:, 0]]
            w_boson = Mom4Vec(data.leptons.mom + data.tru_nu.mom.squeeze())
            m_blv = (blep + w_boson).mass
            plot_corr_heatmaps(
                plot_dir / f"{nu_cut}_vs_mblv",
                m_blv.flatten(),
                qual,
                [np.linspace(110, 230, 50), np.linspace(amin, amax, 50)],
                "Truth " + r"$m_{b \ell \nu}$ [GeV]",
                nu_cut,
                equal_aspect = False,
                incl_line = False,
                do_pdf=False,
            )

        # Cycle through the neutrino reconstruction methods and plot the performance
        for nu_name, (nu, _logp) in nu_data.items():
            pz_rmse = rms(nu.pz - data.tru_nu.pz, axis=-1)
            met = np.repeat(qual[:, np.newaxis], pz_rmse.shape[-1], axis=-1)
            plot_corr_heatmaps(
                pz_rmse.flatten(),
                met.flatten(),
                [np.linspace(0, 600, 50), np.linspace(amin, amax, 50)],
                "RMSE to Truth " + r"$p_z^{\nu}$ " + f"using {nu_name} [GeV]",
                metric,
                plot_dir / f"{metric}_{nu_name}_vs RMSE_pz",
                equal_aspect = False,
                incl_line = False,
                do_pdf=False,
            )

    ## For each of the quality cuts
    for nu_cut in cuts:
        rmse_vals = {nu_name: [] for nu_name in nu_data}

        ## Cycle through the different targetted efficiencies
        for cut_eff in np.linspace(0.1, 0.99, 5):

            ## Get the mask for the cut
            mask = get_quality_mask(nu_data, f"{nu_cut}:{cut_eff}")

            ## Apply the mask to the relevant data
            jets = data.jets[mask]
            jet_idx = data.jet_idx[mask]
            leptons = data.leptons[mask]

            ## Cycle through the neutrino data and build the blv masses
            blvs = {}
            sample_idxs = np.arange(len(jets))
            blep = Mom4Vec(jets.mom[sample_idxs, jet_idx[:, 0]])
            for nu_name, (nu, _logp) in nu_data.items():
                w_boson = Mom4Vec(np.expand_dims(leptons.mom, -2) + nu[mask].mom)
                blvs[nu_name] = Mom4Vec(np.expand_dims(blep.mom, -2) + w_boson.mom)

            ## For each of the blvs corresponding to a neutrino calculate the rmse
            for nu_name in nu_data:
                rmse_vals[nu_name].append(rms(blvs[nu_name].mass - blvs["Truth"].mass))

        ## Add the rmse values for the neutrinos
        ls = next(linecycler)
        for nu_name in nu_data:
            label = nu_name if nu_name != "Truth" else nu_cut
            ax.plot(
                np.linspace(0.1, 1, 5),
                rmse_vals[nu_name],
                label=label,
                color=nu_plot_colour(nu_name),
                linestyle=ls,
            )

    ## Tidy up the plot and save
    ax.set_ylabel("Top Mass RMSE")
    ax.set_xlabel("Cut Efficiency")
    ax.set_xlim(0.1, 1.3)
    ax.set_ylim(0, 100)
    plt.legend()
    plt.savefig("here.png")


def real_matching_plots(
    plot_dir: Path,
    file_path: Path,
    export_dir: Path,
    neutrino_list: list,
    max_jets: int,
    evt_sel: str,
    nu_cut: str,
) -> None:
    """Makes a set of plots using the real associations for the recon
    jets to truth level partons
    """

    ## Load everything once at the start
    data, nu_data = read_and_mask(file_path, export_dir, neutrino_list, max_jets, evt_sel)

    ## Create the plotting folder
    plot_dir = plot_dir / f"{evt_sel}_{max_jets}/{nu_cut}/real_assoc/"
    plot_dir.mkdir(parents=True, exist_ok=True)

    ## Pull out the exact jets seperate arrays
    sample_idxs = np.arange(len(data.jets))

    ## Available if at least one selection is being done
    if evt_sel != "none":
        blep = data.jets[sample_idxs, data.jet_idx[:, 0]]

    ## Only if we are allowing all jets to be matched
    if evt_sel == "all_jets":
        bhad = data.jets[sample_idxs, data.jet_idx[:, 1]]
        w_q1 = data.jets[sample_idxs, data.jet_idx[:, 2]]
        w_q2 = data.jets[sample_idxs, data.jet_idx[:, 3]]
        qq_masses = (w_q1 + w_q2).mass
        bqq_masses = (bhad + w_q1 + w_q2).mass

    ## Each lepton and blep will be repeated for each neutrino, so give that dimension
    data.leptons.mom = np.expand_dims(data.leptons.mom, -2)
    blep.mom = np.expand_dims(blep.mom, -2)

    ## For each neutrino type on the test dataset get the mass distributions
    lvs = []
    blvs = []
    for nu_name, (nu, _logp) in nu_data.items():
        w_boson = data.leptons + nu
        lvs.append(w_boson)
        if evt_sel != "none":
            blvs.append(blep + w_boson)

    ## Plot all of the distributions
    plt.rcParams["lines.linewidth"] = 2

    ## Variables to plot (attr, savename, xlabel, bins, ylim)
    print({nu: np.std(lv.mass - W_MASS) for nu, lv in zip(neutrino_list, lvs)})
    plot_multi_hists(
        [lv.mass.reshape(-1, 1) for lv in lvs],
        [nu_plot_name(nu) for nu in neutrino_list],
        r"$m_{\ell\nu}$ [GeV]",
        path=plot_dir / "lnu_mass",
        logy=True,
        ylim=[1e-4, 5],
        bins=np.linspace(40, 120, 55),
        normed=True,
        incl_overflow=True,
        incl_underflow=True,
        hist_colours=[nu_plot_colour(nu) for nu in neutrino_list],
        hist_kwargs={
            "linestyle": [nu_plot_ls(nu) for nu in neutrino_list],
            "zorder": [10 if nu == "Truth" else 1 for nu in neutrino_list],
        },
        as_pdf=False,
    )

    ## Plot the correlation histograms for the mass and pz
    label = r"$m_{\ell\nu}$ [GeV]"
    for nu, lv in zip(neutrino_list, lvs):
        plot_corr_heatmaps(
            lvs[0].mass.flatten().repeat(lv.mass.shape[1]),
            lv.mass.flatten(),
            np.linspace(60, 100, 30),
            f"Truth {label}",
            f"{nu_plot_name(nu)} {label}",
            plot_dir / ("lnu_mass_" + nu),
            do_pdf=False,
        )

    label = r"$p_z^{l\nu}$ [GeV]"
    for nu, lv in zip(neutrino_list, lvs):
        plot_corr_heatmaps(
            lvs[0].pz.flatten().repeat(lv.pz.shape[1]),
            lv.pz.flatten(),
            np.linspace(-450, 450, 50),
            f"Truth {label}",
            f"{nu_plot_name(nu)} {label}",
            plot_dir / ("lnu_pz_" + nu),
            do_pdf=False,
        )

    if evt_sel != "none":
        print({nu: np.std(blv.mass - TOP_MASS) for nu, blv in zip(neutrino_list, blvs)})
        plot_multi_hists(
            plot_dir / "blnu_mass",
            [blv.mass.reshape(-1, 1) for blv in blvs],
            [nu_plot_name(nu) for nu in neutrino_list],
            r"$m_{b\ell\nu}$ [GeV]",
            multi_hist=multi_hist,
            ylim=[0, 0.06],
            bins=np.linspace(110, 230, 50),
            normed=True,
            incl_overflow=True,
            incl_underflow=True,
            hist_colours=[nu_plot_colour(nu) for nu in neutrino_list],
            hist_kwargs={
                "linestyle": [nu_plot_ls(nu) for nu in neutrino_list],
                "zorder": [10 if nu == "Truth" else 1 for nu in neutrino_list],
            },
            as_pdf=False,
        )

        label = r"$p_z^{bl\nu}$ [GeV]"
        for nu, blv in zip(neutrino_list, blvs):
            plot_corr_heatmaps(
                plot_dir / ("blnu_pz_" + nu),
                blvs[0].pz.flatten().repeat(blv.pz.shape[1]),
                blv.pz.flatten(),
                np.linspace(-450, 450, 50),
                f"Truth {label}",
                f"{nu_plot_name(nu)} {label}",
                do_pdf=False,
            )

        label = r"$m_{b\ell\nu}$ [GeV]"
        for nu, blv in zip(neutrino_list, blvs):
            plot_corr_heatmaps(
                plot_dir / ("blnu_mass_" + nu),
                blvs[0].mass.flatten().repeat(blv.mass.shape[1]),
                blv.mass.flatten(),
                np.linspace(110, 230, 50),
                f"Truth {label}",
                f"{nu_plot_name(nu)} {label}",
                do_pdf=False,
            )

    if evt_sel == "all_jets":

        plot_multi_hists(
            plot_dir / "qq_mass",
            qq_masses,
            "Reco Jets",
            r"$m_{qq}$",
            bins=np.linspace(40, 120, 50),
        )

        plot_multi_hists(
            plot_dir / "bqq_mass",
            bqq_masses,
            "Reco Jets",
            r"$m_{bqq}$",
            bins=np.linspace(100, 220, 50),
        )


def neutrino_kinematics(
    plot_dir: Path,
    file_path: Path,
    export_dir: Path,
    neutrino_list: list,
    max_jets: int,
    evt_sel: str,
    nu_cut: str,
) -> None:
    """Make evaluation plots of the neutrino kinematics"""

    ## Load everything once at the start
    data, nu_data = read_and_mask(file_path, export_dir, neutrino_list, max_jets, evt_sel, nu_cut)

    ## Create the plotting folder
    plot_dir = plot_dir / f"{evt_sel}_{max_jets}/{nu_cut}/neutrinos/"
    plot_dir.mkdir(parents=True, exist_ok=True)

    ## Define the bins for the 1D marginals
    data_bins = {
        r"$p_x^{\nu}$ [GeV]": np.linspace(-150, 150, 30),
        r"$p_y^{\nu}$ [GeV]": np.linspace(-150, 150, 30),
        r"$p_z^{\nu}$ [GeV]": np.linspace(-150, 150, 30),
        r"$E^{\nu}$ [GeV]": np.linspace(0, 200, 30),
    }

    ## Plot the neutrino kinematics for each variable
    for i, var in enumerate(data_bins):

        ## Define the name of the outfile
        outfile = var.translate(str.maketrans("", "", r"^ \{\}\\$[]"))
        outfile = outfile.replace("GeV", "")
        outfile = outfile.replace("nu", "")

        ## Y limits of the plot
        ylim = [0, 0.02]
        if "E" in outfile or "p_z" in outfile:
            rat_ylim = [0.5, 2.1]
        else:
            rat_ylim = [0.65, 1.35]

        plot_multi_hists(
            [nu.mom[..., i].reshape(-1, 1) for _, (nu, _) in nu_data.items()],
            [nu_plot_name(nu) for nu in neutrino_list],
            var,
            plot_dir / outfile,
            ylim=ylim,
            rat_ylim=rat_ylim,
            rat_label="Ratio to Truth",
            normed=True,
            bins=data_bins[var],
            incl_overflow=False,
            incl_underflow=False,
            hist_colours=[nu_plot_colour(nu) for nu in neutrino_list],
            hist_kwargs={
                "linestyle": [nu_plot_ls(nu) for nu in neutrino_list],
                "linewidth": [2 for nu in neutrino_list],
                "zorder": [100 if nu == "Truth" else 1 for nu in neutrino_list],
            },
            do_ratio_to_first=True,
            as_pdf=False,
        )

    ## Redo the binning for the 2D histograms
    data_bins = {
        r"$p_x^{\nu}$ [GeV]": np.linspace(-200, 200, 50),
        r"$p_y^{\nu}$ [GeV]": np.linspace(-200, 200, 50),
        r"$p_z^{\nu}$ [GeV]": np.linspace(-200, 200, 50),
        r"$E^{\nu}$ [GeV]": np.linspace(0, 300, 50),
    }

    ## Plot the correlation histograms for each variable
    for nu_name, (nu, _logp) in nu_data.items():

        ## Repeat the tru_data for the multi sampled methods
        tru_data = np.repeat(data.tru_nu.mom, nu.shape[-2], -2)

        ## For each variable we create a new plot
        for i, var in enumerate(data_bins):

            outfile = f"{var}_{nu_name}"
            outfile = outfile.translate(str.maketrans("", "", r"^ \{\}\\$[]"))
            outfile = outfile.replace("GeV", "")
            outfile = outfile.replace("nu", "")

            plot_corr_heatmaps(
                tru_data[..., i].flatten(),
                nu.mom[..., i].flatten(),
                data_bins[var],
                f"Truth {var}",
                f"{nu_plot_name(nu_name)} {var}",
                plot_dir / outfile,
                do_pearson=False,
                do_pdf=False,
            )


def chi_squared_data(
    file_path: Path,
    neutrino_list: list,
    max_jets: int,
    evt_sel: str,
    nu_cut: str,
) -> None:
    """Makes a data tables using the chi_squared method for associating the recon
    jets to truth level partons
    """

    ## Load everything once at the start
    data, nu_data = read_and_mask(file_path, neutrino_list, max_jets, evt_sel)

    ## Create the folder to save the data
    out_folder = Path(f"data/chi_squared/{evt_sel}_{max_jets}/{nu_cut}/")
    out_folder.mkdir(parents=True, exist_ok=True)

    ## Consider all possible jet permutations (blep, bhad, q1, q2)
    print("Setting up jet permutations")
    perm = list(permutations(np.arange(max_jets)))
    print(f"- number of permutations for {max_jets} jets = {len(perm)}")
    is_sorted = lambda a: np.all(a[:-1] <= a[1:])
    jet_perm = []
    for p in tqdm(perm):
        if not is_sorted(p[2:4]): ## Reject if quadratic W jets are out of order
            continue
        if not is_sorted(p[4:]): ## Reject if non associated jets are out of order
            continue
        jet_perm.append(p)
    print(f"- number of valid permutations = {len(jet_perm)}")

    ## Calculate the masses of the combined qq (N,q,q,mom) and bqq (N,b,q,q,mom)
    qq_options = Mom4Vec(
        data.jets.mom[:, :, np.newaxis, :]  ## q1 jets
        + data.jets.mom[:, np.newaxis, :, :]  ## q2 jets
    ).mass
    bqq_options = Mom4Vec(
        data.jets.mom[:, :, np.newaxis, np.newaxis, :] ## b jets
        + data.jets.mom[:, np.newaxis, :, np.newaxis, :] ## q1 jets
        + data.jets.mom[:, np.newaxis, np.newaxis, :, :]  ## q2 jets
    ).mass

    ## For each neutrino type
    for nu_name, (nu, _lopg) in nu_data:

        ## The min chi squared and the neutrino/jet indicies that match
        min_chisq = np.full(len(nu), np.inf, dtype=np.float32)
        best_neuts = np.zeros(len(nu), dtype=np.int64)
        best_jets = np.zeros((len(nu), max_jets), dtype=np.int64)

        ## Calculate the masses of the combined lv (N,v,mom) and blv (N,b,v,mom)
        lv_options = Mom4Vec(nu.mom + data.leptons.mom[:, np.newaxis, :]).mass
        blv_options = Mom4Vec(
            nu.mom[:, np.newaxis, :, :]
            + data.jets.mom[:, :, np.newaxis, :]
            + data.leptons.mom[:, np.newaxis, np.newaxis, :]
        ).mass

        ## Cycle through all possible neutrinos
        for v in tqdm(range(nu.shape[-2]), leave=False, ncols=100):

            ## Cycle through all possible jet permutations (is blep, bhad, q1, q2, oth)
            for p in tqdm(jet_perm, leave=False, ncols=100):

                ## Select the corresponding slices of the above tensors to use
                lv_mass = np.squeeze(lv_options[:, v])
                blv_mass = np.squeeze(blv_options[:, p[0], v])
                qq_mass = np.squeeze(qq_options[:, p[2], p[3]])
                bqq_mass = np.squeeze(bqq_options[:, p[1], p[2], p[3]])

                ## Calculate the chi squared for this combination of v and p
                chi_sq = (
                    ((lv_mass - W_MASS) / LNU_SIGMA[nu_name]) ** 2
                    + ((blv_mass - TOP_MASS) / BLNU_SIGMA[nu_name]) ** 2
                    + ((qq_mass - W_MASS) / QQ_SIGMA) ** 2
                    + ((bqq_mass - TOP_MASS) / BQQ_SIGMA) ** 2
                )

                ## Replace the values of the min_chi and the best associations
                min_mask = chi_sq < min_chisq  ## Will always be false for Nans
                min_chisq[min_mask] = chi_sq[min_mask]
                best_neuts[min_mask] = v
                best_jets[min_mask] = np.array(p)

        ## Check if the matching was correct (since we ordered the W this should work)
        jet_corr = (data.jet_idx == best_jets[:, :4])
        all_corr = np.sum(jet_corr, axis=-1) == 4
        jet_acc = np.mean(jet_corr, axis=0) * 100
        all_acc = np.mean(all_corr, axis=0) * 100

        ## print the accuracy of the different types
        print(f"{nu_name:<15} -> ", end="")
        for nm, col in zip(["blep", "bhad", "q1", "q2"], jet_acc.T):
            print(f"{nm} = {col:5.2f}, ", end="")
        print(f"All = {all_acc:.2f} -> ", end="")
        print(f"MinChiSq = {min_chisq.mean():.5f}")

        ## Save the outputs as an HDF data table containing: best neutrino, jet perm
        out_hf = h5py.File(out_folder / nu_name, "w")
        out_hf.create_dataset("jet_orders", data=best_jets)
        out_hf.create_dataset("best_neut", data=nu.mom[np.arange(len(nu.mom)), best_neuts])
        out_hf.create_dataset("min_chisq", data=min_chisq)
        out_hf.close()


def main() -> None:
    """Script main function"""

    data_path = Path(
        "/srv/beegfs/scratch/groups/rodem/ttbar_evt_reco/data/new_channels/events_0_selected.h5"
        # "/srv/beegfs/scratch/groups/rodem/ttbar_evt_reco/data_share/20221027_Wlvjjbb_ljets/events_0_selected.h5"
    )
    export_dir = Path("/home/users/l/leighm/scratch/NuFlowOutputs/semilep_ttbar/")
    plot_dir = Path("plots")
    nu_list = [
        "Truth",
        "MET_Quad",
        "60399548_35_NuFlow_1",
        "60399548_35_NuFlow_Mode",
        "60399548_56_BNuFlow_1024",
    ]
    evt_sel = "none"
    max_jets = 10
    metrics = [
        "60399548_56_BNuFlow_mmd",
        "60399548_35_NuFlow_gmm",
    ]

    # Load the metric information
    rmse_vs_cut_plots(plot_dir, data_path, export_dir, nu_list, max_jets, evt_sel, metrics)
    # real_matching_plots(plot_dir, data_path, export_dir, nu_list, max_jets, evt_sel, nu_cut)
    # neutrino_kinematics(plot_dir, data_path, export_dir, nu_list, max_jets, evt_sel, nu_cut)
    # chi_squared_data(data_path, export_dir, nu_list, 5, evt_sel, nu_cut)




if __name__ == "__main__":
    main()
