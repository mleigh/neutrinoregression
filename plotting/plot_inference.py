from pathlib import Path
import h5py

from tqdm import tqdm
import torch as T
import numpy as np
from joblib import load

import matplotlib.pyplot as plt

from mattstools.utils import load_yaml_files
from mattstools.torch_utils import sel_device, move_dev, to_np
from mattstools.bayesian import change_deterministic

from nureg.datasets import NuRegData
from nureg.physics import neut_to_pxpypz, combine_comps
from nureg.plotting import nu_plot_colour

T.set_grad_enabled(False)



def gmm_fits(inputs: np.ndarray, n_gaus: int = 2, only_on_z: bool = False) -> None:
    """Runs a Gaussian Mixture model on each sample and returns the covariance"""
    metrics = []
    gmm_model = GaussianMixture(n_components = n_gaus)
    for sample in inputs:
        gmm = gmm_model.fit(sample)
        metrics.append(np.mean(np.abs(gmm.covariances_*gmm.weights_[:, None, None])))
    return np.vstack(metrics)

def main() -> None:

    ###########################

    ## Create a list of network paths to load and compare
    model_dir = Path(
        "/home/users/l/leighm/scratch/Saved_Networks/NuFlowsRedux/BayesianSearch"
    )
    data_path = Path("/srv/beegfs/scratch/groups/rodem/ttbar_evt_reco/data_share/20221027_Wlvjjbb_ljets")
    plot_folder = Path(f"plots/inference_wjets/")

    flow_name = "60399548_35"
    bflow_name = "60399548_56"

    num_samples = 2048
    num_bflow_passes = 64
    n_bins = 100
    vertical_scaling = 1
    device = sel_device("cpu")

    ###########################

    ## Make the plotting folder
    plot_folder.mkdir(parents=True, exist_ok=True)

    ## Load the networks
    flow = T.load(model_dir / flow_name / "best", map_location=device)
    flow.device = device
    flow.eval()
    bflow = T.load(model_dir / bflow_name / "best", map_location=device)
    bflow.device = device
    bflow.eval()

    ## Load the data using the flow's config file (should be identical for all models)
    data_conf = load_yaml_files(model_dir / flow_name / "config/data.yaml")
    data_conf["path"] = data_path
    del data_conf["variables"]["neutrinos"] # delete the neutrinos for the WJets
    dataset = NuRegData("test", **data_conf)

    ## Before applying the scalars save the truth and the solutions
    sol_1, sol_2 = combine_comps(
        to_np(dataset.comp_1),
        to_np(dataset.comp_2),
        return_eta=True,
        nu_pt=to_np(T.linalg.norm(dataset.data["MET"], dim=-1)),
        return_both=True,
    )
    # truth_nu = dataset.data["neutrinos"]
    met = dataset.data["MET"]

    ## Load and apply the scalers
    scalers = load(model_dir / flow_name / "scalers")
    dataset.apply_preprocess(scalers)

    ## Loop through the samples
    for i in range(20):

        ## Load a single datapoint
        sample = move_dev(dataset[i : i + 1], device)

        ## Generate many outputs for the flow
        generated = (
            flow.generate(sample, n_points=num_bflow_passes * num_samples).cpu().numpy()
        )
        generated = scalers["neutrinos"].inverse_transform(generated)

        ## Generate outputs for the bflow keep the running stats
        b_gen = []
        for b in tqdm(range(num_bflow_passes)):
            gen = bflow.generate(sample, n_points=num_samples).cpu().numpy()
            gen = scalers["neutrinos"].inverse_transform(gen)
            b_gen.append(gen)

        ## Generate nominal values for the bflow
        change_deterministic(bflow, True)
        b_nom = bflow.generate(sample, n_points=num_samples).cpu().numpy()
        b_nom = scalers["neutrinos"].inverse_transform(b_nom)

        ## Create the plots
        plt_vars = [r"$p_x^{\nu}$", r"$p_y^{\nu}$", r"$\eta^{\nu}$"]

        for j, var in enumerate(dataset.out_vars):
            fig, ax = plt.subplots(figsize=(5, 5))
            h = 1.0

            ## Truth is plotted first
            # ax.plot(
            #     [truth_nu[i, j], truth_nu[i, j]],
            #     [0, h],
            #     "--k",
            #     linewidth=2,
            #     label="Truth Neutrino",
            #     zorder=100,
            # )

            ## Then the constraints
            if j < 2:
                ax.plot(
                    [met[i, j], met[i, j]], [0, h], label=r"$p_T^{miss}$", color="C2"
                )
            else:
                ax.plot(
                    [sol_1[i], sol_1[i]],
                    [0, h],
                    color="g",
                    label=r"$\vec{p}_T^\mathrm{miss}+m_W$ Constraint",
                )
                ax.plot(
                    [sol_2[i], sol_2[i]],
                    [0, h],
                    color="C2",
                )

            ## Add a histogram of flow's samples
            flow_hist, bins = np.histogram(generated[:, j], bins=n_bins, density=False)
            mid_bins = bins[1:] - (bins[1] - bins[0]) / 2
            scale = flow_hist.max()
            flow_uncer = np.sqrt(flow_hist)
            flow_hist = flow_hist / scale
            flow_uncer = flow_uncer / scale
            ax.plot(mid_bins, flow_hist, label=r"$\nu$-Flows", color="C1", zorder=-1)

            ## Add statistical uncertainties for the standard flow
            ax.fill_between(
                mid_bins,
                flow_hist - flow_uncer,
                flow_hist + flow_uncer,
                color="C1",
                alpha=0.2,
                zorder=-2,
            )

            ## Add the bflows plots
            bflow_hists = []
            for b in range(num_bflow_passes):
                bflow_hists.append(
                    np.histogram(b_gen[b][:, j], bins=bins, density=False)[0]
                )
            bflow_hists = np.array(bflow_hists)
            bflow_means = np.mean(bflow_hists, axis=0)
            bflow_stt_unc = np.sqrt(bflow_means)
            bflow_tot_unc = np.sqrt(np.var(bflow_hists, axis=0) + bflow_means)
            scale = bflow_hists.max()
            bflow_means = bflow_means / scale
            bflow_stt_unc = bflow_stt_unc / scale
            bflow_tot_unc = bflow_tot_unc / scale

            ## Plot the bflow variance as an error bar
            ax.plot(mid_bins, bflow_means, label=r"B$\nu$-Flows", color="C3")
            ax.fill_between(
                mid_bins,
                bflow_means - bflow_stt_unc,
                bflow_means + bflow_stt_unc,
                color="C3",
                alpha=0.2,
            )
            ax.fill_between(
                mid_bins,
                bflow_means - bflow_tot_unc,
                bflow_means + bflow_tot_unc,
                color="C4",
                alpha=0.2,
            )

            ## Plotting details
            ax.set_title(f"Event {i}: {var} marginal")
            ax.set_ylim([0, 1.6 * h])
            ax.set_xlabel(plt_vars[j])
            ax.set_ylabel("a.u.")
            ax.legend()
            fig.tight_layout()
            fig.savefig(plot_folder / f"{var}_{i}")
            # fig.savefig(plot_folder / f"{var}_{i}.pdf")
            plt.close(fig)


if __name__ == "__main__":
    main()
