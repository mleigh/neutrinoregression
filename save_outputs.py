from pathlib import Path
from joblib import load
from tqdm import tqdm
import numpy as np
import h5py

import torch as T
import torch.nn as nn

from mattstools.utils import load_yaml_files
from mattstools.torch_utils import sel_device, move_dev, to_np
from mattstools.bayesian import change_deterministic

from nureg.datasets import NuRegData
from nureg.physics import neut_to_pxpypz

T.set_grad_enabled(False)

def pass_dataset(
    n_samples: int, network: nn.Module, data: list, batch_size: int = 256
) -> tuple:
    n_events = len(data[0])
    n_batches = int(np.ceil(n_events / batch_size))
    all_outputs = []
    all_log_probs = []
    for b in tqdm(range(n_batches)):
        b_str = b * batch_size
        b_end = min(b_str + batch_size, n_events)
        batch = list(d[b_str:b_end] for d in data)
        outputs, log_probs = network.sample_and_log_prob(batch, n_points=n_samples)
        all_outputs.append(outputs)
        all_log_probs.append(log_probs)
    all_outputs = T.cat(all_outputs).cpu()
    all_log_probs = T.cat(all_log_probs).cpu()

    return all_outputs, all_log_probs


def save_network_outputs(
    model_dir: Path,
    model_name: str,
    flag: str,
    data_folder: Path,
    output_folder: Path,
    n_samples: int = 128,
) -> None:
    """Creates a HDF file containing network outputs and likelihoods given
    a model and some target files
    """

    ## Load the network
    device = sel_device("cuda")
    network = T.load(model_dir / model_name / "best", map_location=device)
    network.device = device
    network.eval()

    ## Check if it is a generative model or a feed forward net
    is_gen = hasattr(network, "sample_and_log_prob")

    ## Load the data using the network's specific config file
    data_conf = load_yaml_files(model_dir / model_name / "config/data.yaml")
    data_conf["path"] = data_folder
    del data_conf["variables"]["neutrinos"] # delete the neutrinos for the WJets
    dataset = NuRegData("test", **data_conf)

    ## Load and apply the scalers, then move to the device
    scalers = load(model_dir / model_name / "scalers")
    dataset.apply_preprocess(scalers)
    data = move_dev(dataset[:], device)

    ## Sample from the network multiple times then undo scaling
    if is_gen:
        outputs, log_probs = pass_dataset(n_samples, network, data)
        outputs = outputs.view(-1, outputs.shape[-1])
        outputs = T.from_numpy(scalers["neutrinos"].inverse_transform(outputs))
        outputs = outputs.view(-1, n_samples, outputs.shape[-1])

    else:
        outputs = network(data[:4])
        outputs = T.from_numpy(scalers["neutrinos"].inverse_transform(outputs))
        outputs = outputs.view(-1, 1, outputs.shape[-1])
        log_probs = T.ones((len(outputs), 1))

    ## For 1D outputs (only longitudinal), add truth x,y
    if outputs.shape[-1] == 1:
        met = T.from_numpy(scalers["MET"].inverse_transform(dataset.data["MET"])).view(
            -1, 1, 2
        )
        outputs = T.cat([met, outputs], dim=-1)
        dataset.out_vars = ["px", "py"] + dataset.out_vars

    ## For 2D outputs add a dummy eta variable
    elif outputs.shape[-1] == 2:
        outputs = T.cat([outputs, T.zeros_like(outputs[..., :1])], dim=-1)
        dataset.out_vars.append("eta")

    ## Convert the outputs to px, py, pz
    outputs = neut_to_pxpypz(outputs, dataset.out_vars)

    ## Save the outputs as a table
    hf = h5py.File(output_folder / f"{model_name}_{flag}", "w")
    hf.create_dataset("gen_neutrinos", data=to_np(outputs))
    hf.create_dataset("log_probs", data=to_np(log_probs))
    hf.close()


def main() -> None:

    ## Create a list of network paths to load and compare
    network_folder = Path(
        "/home/users/l/leighm/scratch/Saved_Networks/NuFlowsRedux/BayesianSearch"
    )
    model_names = [
        ("60399548_35", "NuFlow"),
        ("60399548_56", "BNuFlow"),
    ]

    ## Save the network outputs to an HDF file
    for mn, flg in model_names:
        print(mn)
        save_network_outputs(
            model_dir=network_folder,
            model_name=mn,
            flag=flg,
            data_folder=Path(
                "/srv/beegfs/scratch/groups/rodem/ttbar_evt_reco/data/new_channels/"
            ),
            output_folder=Path("/home/users/l/leighm/scratch/NuFlowOutputs/semilep_ttbar/"),
            n_samples=1024,
        )

if __name__ == "__main__":
    main()
