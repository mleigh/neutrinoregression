"""
Script for evaluating the trained flows
"""

from glob import glob
from pathlib import Path
from tqdm import tqdm
from joblib import load

import numpy as np
import pandas as pd
import torch as T
import torch.nn.functional as F

from mattstools.utils import load_yaml_files
from mattstools.torch_utils import move_dev, to_np, rmse, sel_device

from nureg.datasets import NuRegData
from nureg.physics import neut_to_pxpypz, combine_comps

TOP_MASS = 173
W_MASS = 80.370

T.manual_seed(42)


def main():

    ## Create a list of network paths to load and compare
    network_folder = (
        "/home/users/l/leighm/scratch/Saved_Networks/NuRegFlows/JetPairSearch/"
    )
    file_name = "best"
    net_names = ["*"]

    ## Search for all files and flatten
    paths = [glob(network_folder + net + "/" + file_name) for net in net_names]
    paths = list(np.array(paths).flat)
    paths = [Path(p) for p in paths]

    ## Define the device to work on and set pytorch to not track gradients
    device = sel_device("cpu")
    T.set_grad_enabled(False)

    ## Many steps are repeated, like getting truth values, so we only do this once
    do_all_steps = True
    old_data_conf = {}

    ## Some saved performance results for using MET and quadratic method
    met_perf = [23.25912, 23.203, 1.3977948, 8.387627, 31.12213, 32.23242]

    ## Cycle through each of the networks
    for net_file in tqdm(paths):

        ## Load the data config file and save the output number
        data_conf = load_yaml_files(Path(net_file.parent, "config/data.yaml"))
        if len(data_conf["out_vars"].split(",")) == 2:
            continue

        ## Load the network
        network = T.load(net_file, map_location=device)
        network.device = device
        network.eval()

        ## Load new data if needed, otherwise data is reused
        if data_conf != old_data_conf:
            dataset = NuRegData("test", **data_conf)

            if do_all_steps:

                ## Get the unprocessed tensors from the dataset for the outputs
                raw_neutrinos = dataset.data["neutrinos"]
                raw_leptons = dataset.data["leptons"]
                raw_MET = dataset.data["MET"]

                ## Raw met needs an eta placeholder
                raw_MET = T.cat([raw_MET, T.zeros((len(raw_MET), 1))], dim=-1)

            ## Load and apply the scalers
            scalers = load(open(Path(net_file.parent, "scalers"), "rb"))
            dataset.apply_preprocess(scalers)
            data = move_dev(dataset[:], device)
            old_data_conf = data_conf

        ## Pass the data through the network and convert to numpy (for scalar)
        gen_neutrinos = to_np(network.get_mode(data, n_points=10))

        ## Postprocess the network's outputs using the neutrino scaler
        gen_neutrinos = T.from_numpy(
            scalers["neutrinos"].inverse_transform(gen_neutrinos)
        )

        ## If only used two dimensions, then pad with eta = 0
        if gen_neutrinos.shape[-1] == 2:
            gen_neutrinos = T.cat(
                [gen_neutrinos, T.zeros_like(gen_neutrinos[:, :1])], dim=-1
            )
            dataset.out_vars.append("eta")

            if do_all_steps:
                raw_neutrinos = T.cat(
                    [raw_neutrinos, T.zeros_like(raw_neutrinos[:, :1])], dim=-1
                )

        ## Convert the output data into px, py eta
        met_vars = dataset.lep_vars[:2]
        gen_neutrinos = neut_to_pxpypz(gen_neutrinos, dataset.out_vars, True)
        if do_all_steps:
            raw_neutrinos = neut_to_pxpypz(raw_neutrinos, dataset.out_vars, True)
            raw_MET = neut_to_pxpypz(raw_MET, met_vars + ["eta"], True)
            raw_leptons = neut_to_pxpypz(raw_leptons, dataset.lep_vars, True)

        ## Calculate the conventional eta using quadratic solutions
        if do_all_steps:
            raw_MET[:, 2] = T.from_numpy(
                combine_comps(
                    to_np(dataset.comp_1),
                    to_np(dataset.comp_2),
                    return_eta=True,
                    nu_pt=np.linalg.norm(raw_MET[:, :2], axis=-1),
                )
            )

        ## Calculate the true W mass of the event (using rec lept, truth neuts)
        if do_all_steps:
            tru_w_mass = get_parent(
                [raw_neutrinos, raw_leptons], is_eta=True, only_mass=True
            )

        ## Do the same for the network
        nu_rmse = rmse(gen_neutrinos, raw_neutrinos)
        w_mass = get_parent([gen_neutrinos, raw_leptons], is_eta=True, only_mass=True)
        w_mass_shift = (w_mass.mean() - W_MASS).abs()
        w_mass_rms = w_mass.std()
        w_mass_rmse = rmse(w_mass, tru_w_mass)
        net_perf = to_np(T.hstack([nu_rmse, w_mass_shift, w_mass_rms, w_mass_rmse]))

        ## Save the RMSE to the network directory as a pandas dataset
        pd.DataFrame(
            data=np.vstack([net_perf]),
            index=[str(net_file.parent.name)],
            columns=["px_rmse", "py_rmse", "eta_rmse", "w_shift", "w_rms", "w_rmse"],
        ).to_csv(Path(net_file.parent, "RMSE.csv"))

        do_all_steps = False


if __name__ == "__main__":
    main()
