import h5py
import numpy as np


def merge_hdfs_by_collec(col_dict: dict, dir: str, group_name: str, datasets: list):
    """Merges HDF files together using a dictionary"""

    ## Cycle through each entry in the collection dictionary
    for col, file_list in col_dict.items():

        ## Calculate exactly how many datapoints going to be in the collection
        n_data = 0
        for file_name in file_list:
            with h5py.File(dir + file_name, mode="r") as inpt_file:
                n_data += inpt_file[group_name][datasets[0]].shape[0]

        ## Get the shapes of each dataset
        shapes = [[] for _ in range(len(datasets))]
        with h5py.File(dir + file_list[0], mode="r") as inpt_file:
            for d, dset in enumerate(datasets):
                shapes[d] = inpt_file[group_name][dset].shape[1:]

        ## Open the combined collection
        with h5py.File(dir + col, mode="w") as col_file:

            ## Create the datasets with the correct shape in the collection
            for d, dset in enumerate(datasets):
                col_file.create_dataset(
                    dset,
                    dtype="f",
                    shape=d_shape,
                    maxshape=(None, *d_shape[1:]),
                )

            ## Cycle through and open all input files in the list
            for file_name in file_list:
                with h5py.File(dir + file_name, mode="r") as inpt_file:

                    ## Load the group given by the
                    group = inpt_file[group_name]

                    ## Cycle through each dataset
                    for d, dset in enumerate(datasets):

                        ## Get the shape of the dataset
                        d_shape = group[dset].shape

                        ## If this is the first time entering the dataset, we declare
                        if dset_row[d] == 0:
                            col_file.create_dataset(
                                dset,
                                dtype="f",
                                shape=d_shape,
                                maxshape=(None, *d_shape[1:]),
                            )

                        ## If the dataset can currently fit in the new file
                        if row1 + dslen <= len(h5fw["alldata"]):
                            h5fw["alldata"][row1 : row1 + dslen, :] = arr_data[:]
                        else:
                            h5fw["alldata"].resize((row1 + dslen, cols))
                            h5fw["alldata"][row1 : row1 + dslen, :] = arr_data[:]
                        row1 += dslen


def main():

    output_collections = {
        "train.h5": [
            "events_1_selected.h5",
            "events_2_selected.h5",
            "events_3_selected.h5",
            "events_4_selected.h5",
        ],
        "test.h5": ["events_0_selected.h5"],
        "val.h5": ["events_5_selected.h5"],
    }

    dir = "/mnt/nu_reg/20220204_ttbar_ljets_moreevents/"
    group = "delphes"
    datasets = [
        "MET",
        "jets",
        "leptons",
        "nbjets",
        "neutrinos",
        "njets",
    ]

    merge_hdfs_by_collec(output_collections, dir, group, datasets)


if __name__ == "__main__":
    main()
