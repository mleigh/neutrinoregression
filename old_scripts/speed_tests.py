from nis import cat
from time import time

import torch as T
import torch.nn as nn


def cat_cntxt(tensor, context, module):
    return module(T.cat([tensor, context], -1))


def add_cntxt(tensor, context, module_1, module_2):
    return module_1(tensor) + module_2(context)


def compare_methods(methods, n_tests):

    for nm, m in methods.items():
        ts = time()
        for t in range(n_tests):
            out = m()
        tt = time() - ts
        print(nm, tt)


def main():

    n_tests = 10000
    on_gpu = True
    b_size = 10000
    inpt_dim = 128
    ctxt_dim = 128
    outp_dim = 128

    if on_gpu:
        T.set_default_tensor_type(T.cuda.FloatTensor)

    input = T.rand(b_size, inpt_dim, requires_grad=True)
    context = T.rand(b_size, ctxt_dim, requires_grad=True)

    module0 = nn.Linear(inpt_dim + ctxt_dim, outp_dim)
    module1 = nn.Linear(inpt_dim, outp_dim)
    module2 = nn.Linear(ctxt_dim, outp_dim)

    methods = {
        "add": lambda: add_cntxt(input, context, module1, module2),
        "cat": lambda: cat_cntxt(input, context, module0),
    }

    compare_methods(methods, n_tests)


if __name__ == "__main__":
    main()
