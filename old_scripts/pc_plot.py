"""
Script for evaluating the trained flows
"""

from glob import glob
from pathlib import Path
from tqdm import tqdm

import numpy as np
import pandas as pd

from mattstools.plotting import parallel_plot
from mattstools.utils import load_yaml_files, get_from_dict

TOP_MASS = 173


def build_dataframe(
    folder: str,
    file_name: str,
    net_names: list,
    data_vars: list = None,
    net_vars: list = None,
    train_vars: list = None,
) -> pd.DataFrame:
    """Load performance and config information from many different saved networks
    and return a single dataframe containing all information
    """

    ## Search for all files and flatten
    paths = [glob(folder + net + "/" + file_name) for net in net_names]
    paths = list(np.array(paths).flat)
    paths = [Path(p) for p in paths]

    ## Cycle through each of the networks
    data_list = []
    for rmse_file in tqdm(paths):
        rmse_info = pd.read_csv(rmse_file, nrows=1, index_col=0)

        ## Dont plot those which produced NaNs
        if rmse_info.isnull().values.any():
            print("NAN!")
            continue

        ## Load the configuration columns
        configs = load_yaml_files(
            [
                Path(rmse_file.parent, "config", "data.yaml"),
                Path(rmse_file.parent, "config", "net.yaml"),
                Path(rmse_file.parent, "config", "train.yaml"),
            ]
        )

        ## Load the configuration columns from the saved yaml files
        all_config = {}
        for conf, vars in zip(configs, [data_vars, net_vars, train_vars]):
            for var in vars:
                key_list = var.split("/")
                all_config[key_list[-1]] = get_from_dict(conf, key_list, default=0)

        config_info = pd.DataFrame([all_config])
        config_info.index = rmse_info.index

        data_list.append(pd.concat([config_info, rmse_info], axis=1))

    ## Combine and return as a single dataframe
    return pd.concat(data_list)


def main():

    ## Create a list of network paths to load and compare
    folder = "/home/users/l/leighm/scratch/Saved_Networks/NuRegFlows/2D3DCDSSearch/"
    file_name = "RMSE.csv"
    net_names = ["*"]

    ## The list of all values from each config to add to the plot
    data_vars = [
        "out_vars",
        "lep_vars",
        "jet_vars",
    ]
    net_vars = [
        "flow_kwargs/net_kwargs/nrm",
        "flow_kwargs/net_kwargs/drp",
        # "flow_kwargs/nstacks",
        # "flow_kwargs/param_func",
        # "flow_kwargs/net_kwargs/nrm",
    ]
    train_vars = [
        # "optim_dict/lr",
        # "b_size"
    ]

    ## Get the dataframe from many different sources and save for quick pickup next time
    # dataframe = build_dataframe(
    #     folder, file_name, net_names, data_vars, net_vars, train_vars
    # )
    # dataframe.to_csv(f"./plots/{Path(folder).name}_parallel_data.csv")

    ## Load a previous dataframe
    dataframe = pd.read_csv(
        f"./plots/{Path(folder).name}_parallel_data.csv", index_col=0
    )

    print(dataframe)
    # print(dataframe.iloc[np.argmin(dataframe["eta_rmse"])].name)
    # print(dataframe.iloc[np.argmin(dataframe["px_rmse"])].name)

    ## Filter the dataframe to show only the better values
    dataframe = dataframe[dataframe.w_rmse < 8]  ## MET rmse
    # dataframe = dataframe[dataframe.py_rmse < 23.5] ## MET rmse
    # dataframe = dataframe[dataframe.pool_type == "attn"]

    print(dataframe.iloc[np.argmin(dataframe["px_rmse"])].name)

    parallel_plot(
        path=f"./plots/{Path(folder).name}",
        df=dataframe,
        cols=list(dataframe.columns),
        rank_col="px_rmse",
        # groupby_methods=["min", "mean"],
        curved=True,
        highlight_best=True,
        do_sort=False,
        class_thresh=5,
    )

    "57347131_3"


if __name__ == "__main__":
    main()
