import numpy as np
import matplotlib.pyplot as plt

from nureg.datasets import NuRegData
from mattstools.plotting import plot_multi_hists

import torch as T


def main():

    ## Define the different pt thresholds for the leptons
    pt_thresh = [0, 50, 100, 150, 200]

    ## Load the dataset
    train_set = NuRegData(
        dset="train", path="/mnt/nu_reg/20220204_ttbar_ljets_moreevents/"
    )

    ## Make a plot of the leptons for the different pt thresholds
    # lept_xy = train_set.group_data["leptons"][:, :2]
    # lept_pt = T.norm(lept_xy, dim=-1)

    # ## Plot the xy distributions for each pt threshold
    # xy_cuts = [lept_xy[lept_pt > tr] for tr in pt_thresh]
    # plot_multi_hists(
    #     "./plots/debug_xy",
    #     xy_cuts,
    #     list(map(str, pt_thresh)),
    #     ["px", "py"],
    #     bins=np.linspace(-300, 300, 50),
    # )

    ## Get the jet px and py for each index
    jet_xy = train_set.data["jets"][..., :2]
    jet_xy = [x.squeeze() for x in T.split(jet_xy, 1, dim=-2)]
    plot_multi_hists(
        "./plots/debug_jets",
        jet_xy,
        np.arange(len(jet_xy)),
        ["px", "py"],
        bins=np.linspace(-400, 400, 200),
        logy=True,
        normed=False,
        incl_zeros=False,
        ylim=[10, 1e5],
    )


if __name__ == "__main__":
    main()
