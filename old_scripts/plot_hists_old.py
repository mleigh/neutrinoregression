"""
Script for plotting distributions from a single flow
"""

from collections import defaultdict
from itertools import permutations
from pathlib import Path
from typing import Tuple

import h5py
import matplotlib.pyplot as plt
import numpy as np
import numpy.lib.recfunctions as rf
import torch as T
from mattstools.plotting import plot_multi_hists, plot_corr_heatmaps
from tqdm import tqdm

from nureg.physics import (
    Mom4Vec,
    combine_comps,
    nu_sol_comps,
)
from nureg.plotting import nu_plot_name, nu_plot_colour, nu_plot_ls

TOP_MASS = 173
W_MASS = 80.379
QQ_SIGMA = 18.07
BQQ_SIGMA = 27.17

LNU_SIGMA = defaultdict(
    lambda: 24.84,
    {
        "Truth": 4.6781764,
        "MET_Quad": 31.117254,
        "NuReg_FFNoQuad_1": 15.32697,
        "NuReg_FlowNoQuad_1": 5.6434116,
        "NuReg_FlowNoQuad_Mode": 1.287631,
    },
)

BLNU_SIGMA = defaultdict(
    lambda: 33.04,
    {
        "Truth": 17.334011,
        "MET_Quad": 50.925045,
        "NuReg_FFNoQuad_1": 25.995289,
        "NuReg_FlowNoQuad_1": 33.672394,
        "NuReg_FlowNoQuad_Mode": 24.807196,
    },
)


T.set_grad_enabled(False)


def get_neutrinos(
    nu: str,
    tru_nu: Mom4Vec,
    met: Mom4Vec,
    njet_sel: np.ndarray,
    mask: np.ndarray = None,
) -> Mom4Vec:
    """Return an Mom4Vec object containing neutrinos specified by the nu string"""
    nu_type = nu.split("_")

    ## The network neutrinos
    if nu_type[0] == "NuReg":

        ## Need to be loaded from file
        net_nu, net_logp = read_net_and_mask("_".join(nu_type[:2]), njet_sel, mask=mask)

        if nu_type[2] == "Mode":
            idxs = np.arange(len(net_nu.mom))
            data = Mom4Vec(net_nu.mom[idxs, np.newaxis, np.argmax(net_logp, axis=-1)])
        else:
            data = Mom4Vec(net_nu.mom[:, 0 : int(nu_type[2])])

        ## Combining with truth for the final dimension
        if nu_type[-1] == "Truth":
            data = Mom4Vec(
                np.concatenate(
                    [
                        data.mom[..., :2],
                        np.repeat(tru_nu.mom[..., 2:3], data.mom.shape[-2], -2),
                    ],
                    axis=-1,
                ),
                n_mom=3,
            )

        ## Combining with quad for the final dimension
        if nu_type[-1] == "Quad":
            data = Mom4Vec(
                np.concatenate(
                    [
                        np.repeat(data.mom[..., :2], 2, -2),
                        np.tile(met.mom[..., 2:3], (1, data.mom.shape[-2], 1)),
                    ],
                    axis=-1,
                ),
                n_mom=3,
            )

        return data

    ## The MET based neutrinos (already contain quad solutions)
    if nu_type[0] == "MET":
        if nu_type[1] == "Quad":
            return met
        elif nu_type[1] == "Truth":
            return Mom4Vec(
                np.concatenate([met.mom[:, :1, :2], tru_nu.mom[..., 2:3]], axis=-1),
                n_mom=3,
            )

    ## For truth neutrinos
    if nu_type[0] == "Truth":
        return tru_nu

    raise ValueError(f"Dont know what to do with neutrino type: {nu}")


def rms(arr: np.ndarray, axis: int = 0) -> np.ndarray:
    """Returns RMS of a numpy array along a dimension"""
    return np.sqrt(np.mean(arr * arr, axis=axis))


def read_singlelep_file(file_path, max_jets=None):
    """Reads in data from a single HDF file"""
    with h5py.File(file_path, "r+") as f:
        ## Read in the particle tensors
        met = rf.structured_to_unstructured(f["delphes"]["MET"][:])
        leptons = rf.structured_to_unstructured(f["delphes"]["leptons"][:])
        jets = rf.structured_to_unstructured(f["delphes"]["jets"][:])
        tru_nu = rf.structured_to_unstructured(f["delphes"]["neutrinos"][:])

        ## Read in the truth association information
        indices = f["delphes"]["jets_indices"][:]
        match = f["delphes"]["matchability"][:]
        channel = f["delphes"]["decay_channel"][:]

    ## Drop neutrino PDGID and add extra dimension (for nu options)
    tru_nu = np.expand_dims(tru_nu[..., 1:], -2)

    ## Count the number of jets in each event
    n_jets = np.sum(jets[..., 0] > 0, axis=-1).astype(np.int64)

    ## Apply an event selection based on the number of jets
    if max_jets is not None:
        sel = n_jets <= max_jets
        met = met[sel]
        leptons = leptons[sel]
        jets = jets[sel]
        tru_nu = tru_nu[sel]
        indices = indices[sel]
        match = match[sel]
        channel = channel[sel]
        n_jets = n_jets[sel]

        ## Manually trim the jet info to only n many jets
        jets = jets[:, :max_jets]
        indices = indices[:, :max_jets]
    else:
        sel = np.ones_like(channel).astype(bool)

    ## Change the jet padding to nans so they wont show up in min chi square!
    jets[jets[..., 0] == 0] = np.nan

    return met, leptons, jets, tru_nu, indices, match, channel, n_jets, sel


def important_indices(
    indices: np.ndarray, channel: np.ndarray, nf_value: int = 9999
) -> np.ndarray:
    """Returns the indices of the blep, bhad, q1, q2 in the jet data table

    If these particular jets are NOT in the jet data table, then the returned index
    takes the nf_value

    args:
        indices: The jets_indices table, of shape (N,10)
        channel: The decay channel of each event
    returns:
        jet_idx: The locations of the important jets in the table, shape (N,4)
    """

    ## Get the "indices" (rep in the jet_indices table) of the different jet types
    blep_idx = np.expand_dims(np.where(channel == 1, 0, 3), -1)
    bhad_idx = np.expand_dims(np.where(channel == 1, 1, 0), -1)
    w_q1_idx = np.expand_dims(np.where(channel == 1, 2, 1), -1)
    w_q2_idx = np.expand_dims(np.where(channel == 1, 3, 2), -1)

    ## Check which jet in the jet table belongs to which type
    is_blep = indices == blep_idx
    is_bhad = indices == bhad_idx
    is_w_q1 = indices == w_q1_idx
    is_w_q2 = indices == w_q2_idx

    ## Check if the event has these types
    has_blep = np.any(is_blep, axis=1)
    has_bhad = np.any(is_bhad, axis=1)
    has_w_q1 = np.any(is_w_q1, axis=1)
    has_w_q2 = np.any(is_w_q2, axis=1)

    ## If the event has the jet, return its index in the jet table, otherwise return nf
    blep_idx = np.where(has_blep, np.argmax(is_blep, axis=1), nf_value)
    bhad_idx = np.where(has_bhad, np.argmax(is_bhad, axis=1), nf_value)
    w_q1_idx = np.where(has_w_q1, np.argmax(is_w_q1, axis=1), nf_value)
    w_q2_idx = np.where(has_w_q2, np.argmax(is_w_q2, axis=1), nf_value)

    ## Define the jet indices as a single tensor (order the W's so q1 comes first)
    jet_idx = np.vstack(
        [
            blep_idx,
            bhad_idx,
            np.minimum(w_q1_idx, w_q2_idx),
            np.maximum(w_q1_idx, w_q2_idx),
        ]
    ).T

    return jet_idx


def evt_selection(sel: str, match: np.ndarray, channel: np.ndarray) -> np.ndarray:
    """Return a mask for using the matchability array and a given selection"""

    ## Check the selection string validity
    if sel not in ["none", "blep", "all_jets"]:
        raise ValueError(f"Unrecognized event selection option: {sel}")

    ## Apply the appropriate selection and return the masking tensor
    if sel == "none":
        return np.ones_like(match, dtype=bool)

    if sel == "blep":
        has_jet_0 = (np.right_shift(match, 5) & 1).astype(bool)
        has_jet_3 = (np.right_shift(match, 2) & 1).astype(bool)
        return np.where(channel == 1, has_jet_0, has_jet_3)

    if sel == "all_jets":
        return match == 63


def fix_decay_channel(paths: list):
    """Fixes the saved decay channels table found in delphes HDF files"""
    for file in paths:
        with h5py.File(file, "r+") as f:

            ## Get the identification information of the W decays from the file
            parton_id = f["delphes"]["partons"]["PDGID"]
            wplus_daughter = parton_id[:, 3]
            wminus_daughter = parton_id[:, 8]

            ## Calculate the correct channel data based on which W decayed into a lepton
            channel = 0
            channel += np.abs(wplus_daughter) > 10
            channel += 2 * (np.abs(wminus_daughter) > 10)

            ## Replace the data in the file with this new array
            channel_data = f["delphes"]["decay_channel"]
            channel_data[...] = channel


def real_matching_plots(file_path: Path, neutrino_list: list, evt_sel: str):
    """Makes a set of plots using the real associations for the recon
    jets to truth level partons

    args:
        file_path: The name of the datafile to run over
        network_name: The name of the neural network to receive the outputs
        evt_sel: string indicating the event selection for jet presence
    """

    ## Read in all the data
    met, leptons, jets, tru_nu, _, jet_idx, njet_sel, sel = read_and_mask(
        file_path, 10, evt_sel
    )
    nu_data = [
        get_neutrinos(nu, tru_nu, met, njet_sel, mask=sel).mom for nu in neutrino_list
    ]

    ## Pull out the exact jets seperate arrays
    sample_idxs = np.arange(len(jets))

    if evt_sel != "none":
        blep = Mom4Vec(jets.mom[sample_idxs, jet_idx[:, 0]])

    if evt_sel == "all_jets":
        bhad = Mom4Vec(jets.mom[sample_idxs, jet_idx[:, 1]])
        w_q1 = Mom4Vec(jets.mom[sample_idxs, jet_idx[:, 2]])
        w_q2 = Mom4Vec(jets.mom[sample_idxs, jet_idx[:, 3]])
        qq_masses = Mom4Vec(w_q1.mom + w_q2.mom).mass
        bqq_masses = Mom4Vec(bhad.mom + w_q1.mom + w_q2.mom).mass

    ## For each neutrino type on the test dataset get the mass distributions
    lvs = []
    blvs = []
    for data in nu_data:

        ## Get the w boson by combining with the lepton
        w_boson = Mom4Vec(np.expand_dims(leptons.mom, -2) + data)

        ## Add the flattened invariant masses to the list
        lvs.append(w_boson)
        if evt_sel != "none":
            blvs.append(Mom4Vec(np.expand_dims(blep.mom, -2) + w_boson.mom))

    ## Plot all of the distributions
    plot_folder = Path(f"plots/real_assoc/{evt_sel}/")
    plot_folder.mkdir(parents=True, exist_ok=True)
    plt.rcParams["lines.linewidth"] = 2

    ## Variables to plot (attr, savename, xlabel, bins, ylim)
    print({nu: np.std(lv.mass - W_MASS) for nu, lv in zip(neutrino_list, lvs)})
    plot_multi_hists(
        plot_folder / "lnu_mass",
        [lv.mass for lv in lvs],
        [nu_plot_name(nu) for nu in neutrino_list],
        r"$m_{\ell\nu}$ [GeV]",
        logy=True,
        ylim=[1e-4, 10],
        bins=np.linspace(40, 120, 55),
        normed=False,
        incl_overflow=True,
        incl_underflow=True,
        hist_colours=[nu_plot_colour(nu) for nu in neutrino_list],
        hist_kwargs={
            "linestyle": [nu_plot_ls(nu) for nu in neutrino_list],
            "zorder": [10 if nu == "Truth" else 1 for nu in neutrino_list],
        },
        hist_scale=1 / len(met),
        as_pdf=True,
    )

    ## Plot the correlation histograms for the mass and pz
    label = r"$m_{\ell\nu}$ [GeV]"
    for nu, lv in zip(neutrino_list, lvs):
        plot_corr_heatmaps(
            plot_folder / ("lnu_mass_" + nu),
            lvs[0].mass.flatten(),
            lv.mass.flatten(),
            np.linspace(70, 90, 30),
            f"Truth {label}",
            f"{nu_plot_name(nu)} {label}",
            do_pdf=True,
        )

    label = r"$p_z^{l\nu}$ [GeV]"
    for nu, lv in zip(neutrino_list, lvs):
        plot_corr_heatmaps(
            plot_folder / ("lnu_pz_" + nu),
            lvs[0].pz.flatten(),
            lv.pz.flatten(),
            np.linspace(-450, 450, 50),
            f"Truth {label}",
            f"{nu_plot_name(nu)} {label}",
            do_pdf=True,
        )

    if evt_sel != "none":
        print({nu: np.std(blv.mass - TOP_MASS) for nu, blv in zip(neutrino_list, blvs)})
        plot_multi_hists(
            plot_folder / "blnu_mass",
            [blv.mass for blv in blvs],
            [nu_plot_name(nu) for nu in neutrino_list],
            r"$m_{b\ell\nu}$ [GeV]",
            ylim=[0, 0.14],
            bins=np.linspace(110, 230, 50),
            normed=False,
            incl_overflow=True,
            incl_underflow=True,
            hist_colours=[nu_plot_colour(nu) for nu in neutrino_list],
            hist_kwargs={
                "linestyle": [nu_plot_ls(nu) for nu in neutrino_list],
                "zorder": [10 if nu == "Truth" else 1 for nu in neutrino_list],
            },
            hist_scale=1 / len(met),
            as_pdf=True,
        )

        label = r"$p_z^{bl\nu}$ [GeV]"
        for nu, blv in zip(neutrino_list, blvs):
            plot_corr_heatmaps(
                plot_folder / ("blnu_pz_" + nu),
                blvs[0].pz.flatten(),
                blv.pz.flatten(),
                np.linspace(-450, 450, 50),
                f"Truth {label}",
                f"{nu_plot_name(nu)} {label}",
                do_pdf=True,
            )

        label = r"$m_{b\ell\nu}$ [GeV]"
        for nu, blv in zip(neutrino_list, blvs):
            plot_corr_heatmaps(
                plot_folder / ("blnu_mass_" + nu),
                blvs[0].mass.flatten(),
                blv.mass.flatten(),
                np.linspace(110, 230, 50),
                f"Truth {label}",
                f"{nu_plot_name(nu)} {label}",
                do_pdf=True,
            )

    if evt_sel == "all_jets":

        plot_multi_hists(
            plot_folder / "qq_mass",
            qq_masses,
            "Reco Jets",
            r"$m_{qq}$",
            bins=np.linspace(40, 120, 50),
        )

        plot_multi_hists(
            plot_folder / "bqq_mass",
            bqq_masses,
            "Reco Jets",
            r"$m_{bqq}$",
            bins=np.linspace(100, 220, 50),
        )


def neutrino_kinematics(file_path: Path, neutrino_list: list, evt_sel: str = "none"):
    """Make evaluation plots of the neutrino kinematics

    args:
        file_path: The name of the datafile to run over
        network_name: The name of the neural network to receive the outputs
    kwargs:
        evt_sel: event selection to apply
    """

    ## Define the bins for the heatmaps
    data_bins = {
        r"$p_x^{\nu}$ [GeV]": np.linspace(-150, 150, 30),
        r"$p_y^{\nu}$ [GeV]": np.linspace(-150, 150, 30),
        r"$p_z^{\nu}$ [GeV]": np.linspace(-150, 150, 30),
        r"$E^{\nu}$ [GeV]": np.linspace(0, 200, 30),
    }

    ## Read in all the data
    met, _, _, tru_nu, _, _, njet_sel, sel = read_and_mask(file_path, 10, evt_sel)
    nu_data = [
        get_neutrinos(nu, tru_nu, met, njet_sel, mask=sel).mom for nu in neutrino_list
    ]

    ## Create the plotting folder
    plot_dir = Path("plots/neutrinos/")
    plot_dir.mkdir(parents=True, exist_ok=True)

    ## Plot the neutrino kinematics for each variable
    for i, var in enumerate(data_bins):

        ## Define the name of the outfile
        outfile = var.translate(str.maketrans("", "", r"^ \{\}\\$[]"))
        outfile = outfile.replace("GeV", "")
        outfile = outfile.replace("nu", "")

        if "E" in outfile:
            ylim = [0, 0.13]
        else:
            ylim = [0, 0.15]

        if "E" in outfile or "p_z" in outfile:
            rat_ylim = [0.5, 2.1]
        else:
            rat_ylim = [0.65, 1.35]

        plot_multi_hists(
            plot_dir / outfile,
            [np.reshape(data[..., i], (-1, 1)) for data in nu_data],
            [nu_plot_name(nu) for nu in neutrino_list],
            var,
            ylim=ylim,
            rat_ylim=rat_ylim,
            rat_label="Ratio to Truth",
            normed=False,
            bins=data_bins[var],
            incl_overflow=False,
            incl_underflow=False,
            hist_scale=1 / len(met),
            hist_colours=[nu_plot_colour(nu) for nu in neutrino_list],
            hist_kwargs={
                "linestyle": [nu_plot_ls(nu) for nu in neutrino_list],
                "linewidth": [2 for nu in neutrino_list],
                "zorder": [100 if nu == "Truth" else 1 for nu in neutrino_list],
            },
            do_ratio_to_first=True,
            as_pdf=True,
        )

    ## Redo the binning
    data_bins = {
        r"$p_x^{\nu}$ [GeV]": np.linspace(-200, 200, 50),
        r"$p_y^{\nu}$ [GeV]": np.linspace(-200, 200, 50),
        r"$p_z^{\nu}$ [GeV]": np.linspace(-200, 200, 50),
        r"$E^{\nu}$ [GeV]": np.linspace(0, 300, 50),
    }

    ## Plot the correlation histograms for each variable
    for nu, data in zip(neutrino_list, nu_data):

        ## Repeat the tru_data for the multi sampled methods
        tru_data = np.repeat(tru_nu.mom, data.shape[-2], -2)

        ## For each variable we create a new plot
        for i, var in enumerate(data_bins):

            outfile = f"{var}_{nu}"
            outfile = outfile.translate(str.maketrans("", "", r"^ \{\}\\$[]"))
            outfile = outfile.replace("GeV", "")
            outfile = outfile.replace("nu", "")

            plot_corr_heatmaps(
                plot_dir / outfile,
                tru_data[..., i].flatten(),
                data[..., i].flatten(),
                data_bins[var],
                f"Truth {var}",
                f"{nu_plot_name(nu)} {var}",
                do_pearson=False,
                do_pdf=True,
            )


def chi_squared_data(
    file_path: Path,
    neutrino_list: list,
    max_jets: int = 4,
    evt_sel: str = "none",
):
    """Makes a data tables using the chi_squared method for associating the recon
    jets to truth level partons

    args:
        file_path: The name of the datafile to run over
        network_name: The name of the neural network to receive the outputs
    kwargs:
        max_jets: the number of jets to use in the possible permutations
        evt_sel: Requirements of the presence of truth jets in the jet container
    """

    ## Read in all the data
    met, leptons, jets, tru_nu, _, jet_idx, njet_sel, sel = read_and_mask(
        file_path, max_jets, evt_sel
    )
    nu_data = [
        get_neutrinos(nu, tru_nu, met, njet_sel, mask=sel).mom for nu in neutrino_list
    ]

    ## Consider all possible jet permutations (blep, bhad, q1, q2)
    print("Setting up jet permutations")
    perm = list(permutations(np.arange(max_jets)))
    print(f"- number of permutations for {max_jets} jets = {len(perm)}")
    is_sorted = lambda a: np.all(a[:-1] <= a[1:])
    jet_perm = []
    for p in perm:

        ## Reject if quadratic W jets are out of order
        if not is_sorted(p[2:4]):
            continue

        ## Reject if non associated jets are out of order
        if not is_sorted(p[4:]):
            continue

        jet_perm.append(p)
    print(f"- number of valid permutations = {len(jet_perm)}")

    ## Calculate the masses of the combined qq (N,q,q,mom) and bqq (N,b,q,q,mom)
    qq_options = Mom4Vec(
        jets.mom[:, :, np.newaxis, :]  ## q1 jets
        + jets.mom[:, np.newaxis, :, :]  ## q2 jets
    ).mass
    bqq_options = Mom4Vec(
        jets.mom[:, :, np.newaxis, np.newaxis, :]
        + jets.mom[:, np.newaxis, :, np.newaxis, :]  ## b jets
        + jets.mom[:, np.newaxis, np.newaxis, :, :]  ## q1 jets  ## q2 jets
    ).mass

    ## For each neutrino type
    for nu_type, data in zip(neutrino_list, nu_data):
        print(nu_type)

        ## The min chi squared and the neutrino/jet indicies that match
        min_chisq = np.full(len(data), np.inf, dtype=np.float32)
        best_neuts = np.zeros(len(data), dtype=np.int64)
        best_jets = np.zeros((len(data), max_jets), dtype=np.int64)

        ## Calculate the masses of the combined lv (N,v,mom) and blv (N,b,v,mom)
        lv_options = Mom4Vec(np.expand_dims(leptons.mom, -2) + data).mass
        blv_options = Mom4Vec(
            jets.mom[:, :, np.newaxis, :]
            + leptons.mom[:, np.newaxis, np.newaxis, :]
            + data[:, np.newaxis, :, :]
        ).mass

        ## Cycle through all possible neutrinos
        for v in tqdm(range(data.shape[-2]), leave=False, ncols=100):

            ## Cycle through all possible jet permutations (is blep, bhad, q1, q2, oth)
            for p in tqdm(jet_perm, leave=False, ncols=100):

                ## Select the corresponding slices of the above tensors to use
                lv_mass = np.squeeze(lv_options[:, v])
                blv_mass = np.squeeze(blv_options[:, p[0], v])
                qq_mass = np.squeeze(qq_options[:, p[2], p[3]])
                bqq_mass = np.squeeze(bqq_options[:, p[1], p[2], p[3]])

                ## Calculate the chi squared for this combination of v and p
                chi_sq = (
                    ((lv_mass - W_MASS) / LNU_SIGMA[nu_type]) ** 2
                    + ((blv_mass - TOP_MASS) / BLNU_SIGMA[nu_type]) ** 2
                    + ((qq_mass - W_MASS) / QQ_SIGMA) ** 2
                    + ((bqq_mass - TOP_MASS) / BQQ_SIGMA) ** 2
                )

                ## Replace the values of the min_chi and the best associations
                min_mask = chi_sq < min_chisq  ## Will always be false for Nans
                min_chisq[min_mask] = chi_sq[min_mask]
                best_neuts[min_mask] = v
                best_jets[min_mask] = np.array(p)

        ## Check if the matching was correct (since we ordered the W this should work)
        jet_corr = jet_idx == best_jets[:, :4]
        all_corr = np.sum(jet_corr, axis=-1) == 4
        jet_acc = np.mean(jet_corr, axis=0) * 100
        all_acc = np.mean(all_corr, axis=0) * 100

        ## print the accuracy of the different types
        # print(f"{nu_type:<15} -> ", end="")
        for nm, col in zip(["blep", "bhad", "q1", "q2"], jet_acc.T):
            print(f"{nm} = {col:5.2f}, ", end="")
        print(f"All = {all_acc:.2f} -> ", end="")
        print(f"MinChiSq = {min_chisq.mean():.5f}")

        ## Save the outputs as an HDF data table containing: best neutrino, jet perm
        data_folder = Path(f"data/chi_squared_flex/{evt_sel}/")
        data_folder.mkdir(parents=True, exist_ok=True)
        out_hf = h5py.File(data_folder / nu_type, "w")
        out_hf.create_dataset("jet_orders", data=best_jets)
        out_hf.create_dataset("best_neut", data=data[np.arange(len(data)), best_neuts])
        out_hf.create_dataset("min_chisq", data=min_chisq)
        out_hf.close()


def read_and_mask(file_path: str, max_jets: int, evt_sel: str) -> tuple:
    """Read in data from the HDF file and mask based on the max number of jets
    and a particular event selection using the matchability and channel variables
    """

    ## Read in all of the important data from the hdf file
    (
        met,
        leptons,
        jets,
        tru_nu,
        indices,
        match,
        channel,
        n_jets,
        njet_sel,
    ) = read_singlelep_file(file_path, max_jets)

    ## Derive and apply a mask for the dataset
    sel = evt_selection(evt_sel, match, channel)
    indices = indices[sel]
    match = match[sel]
    channel = channel[sel]
    n_jets = n_jets[sel]
    met = met[sel]
    leptons = leptons[sel]
    jets = jets[sel]
    tru_nu = tru_nu[sel]

    ## Get the indices of the blep, bhad, q1, q2 in the event to use in the jets table
    jet_idx = important_indices(indices, channel)

    ## Wrap the arrays into the mom4vec class
    met = Mom4Vec(met, is_cartesian=False, n_mom=2)
    leptons = Mom4Vec(leptons, is_cartesian=False)
    jets = Mom4Vec(jets, is_cartesian=False)
    tru_nu = Mom4Vec(tru_nu, is_cartesian=False, n_mom=3)

    ## Convert to cartesian
    met.to_cartesian()
    leptons.to_cartesian()
    jets.to_cartesian()
    tru_nu.to_cartesian()

    ## Add the quadratic solutions to the MET neutrinos
    sol_1 = combine_comps(
        *nu_sol_comps(
            leptons.px,
            leptons.py,
            leptons.pz,
            leptons.energy,
            leptons.oth[..., -1:],  ## lepton ID type is stored in final variable
            met.px,
            met.py,
        ),
        return_both=False,
    )
    met.mom = np.expand_dims(met.mom, -2)
    met.mom[:, 0, 2:3] = sol_1
    # met.mom = np.repeat(np.expand_dims(met.mom, -2), 2, -2)
    # met.mom[:, 0, 2:3] = sol_1
    # met.mom[:, 1, 2:3] = sol_2
    met.set_massless_energy()

    return met, leptons, jets, tru_nu, n_jets, jet_idx, njet_sel, sel


def read_net_and_mask(
    network_name: str, njet_sel: np.ndarray, mask: np.ndarray = None
) -> Tuple[Mom4Vec, np.ndarray]:
    """Read the outputs of a network and mask based on number of jets and evt sel"""

    ## Load the file containing the network outputs (load only njet sel evnts)
    with h5py.File(Path("data") / network_name, "r") as f:
        net_nu = f["gen_neutrinos"][:][njet_sel]
        net_logp = f["log_probs"][:][njet_sel]

    ## Apply the event selection masking and convert to cartesian
    if mask is not None:
        net_nu = net_nu[mask]
        net_logp = net_logp[mask]

    ## Convert to a cartisian momentum 4 vector
    net_nu = Mom4Vec(net_nu, is_cartesian=True, n_mom=3)
    net_nu.to_cartesian()

    return net_nu, net_logp


def main():
    """Script main function"""

    neutrino_list = [
        "Truth",
        "MET_Quad",
        # "NuReg_FF3_1",
        # "NuReg_TMask_1",
        # "NuReg_TMask_Mode",
        # "NuReg_TMaskB_1",
        # "NuReg_TMaskB_Mode",1
        # "NuReg_TMaskBLong_Mode",
        "NuReg_FFNoQuad_1",
        "NuReg_FlowNoQuad_1",
        "NuReg_FlowNoQuad_Mode",
    ]

    data_path = Path("data/events_0_selected.h5")

    ## Make the mass plots using the jet associations
    # real_matching_plots(data_path, neutrino_list, "none")
    real_matching_plots(data_path, neutrino_list, "blep")
    # neutrino_kinematics(data_path, neutrino_list, "none")

    ## Save the chi_squared information
    # for e_sel in ["all_jets", "blep"]:
    #     chi_squared_data(data_path, neutrino_list, max_jets=10, evt_sel=e_sel)


if __name__ == "__main__":
    main()
