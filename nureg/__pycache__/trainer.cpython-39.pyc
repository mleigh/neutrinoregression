a
    ���aS*  �                   @   s�   d Z ddlmZ ddlZddlZddlmZ ddlZddl	m
Z
 ddlmZmZ ddlmZmZmZmZ ddlmZ G dd	� d	�ZdS )
z!
Base class for training network
�    )�PathN)�tqdm)�Dataset�
DataLoader)�RunningAverage�	get_optim�	get_sched�move_dev)�plot_multi_lossc                   @   s�   e Zd ZdZdeeeeeeeeeedd�d	d
�Z	dd�dd�Z
dedd�dd�Zdd�dd�Zdd�dd�Zddd�dd�ZdS )�TrainerzLA class to oversee the training of a network which can handle its own losses�    �d   �        �   NT)�	train_set�	valid_set�b_size�patience�
max_epochs�	grad_clip�	n_workers�
optim_dict�
sched_dict�vis_on_save�returnc              	   C   sN  t d� |	pddd�}	|
p ddi}
|| _t dt|�d�d	�� |rZt d
t|�d�d	�� nt d� ||ddd�}t|d�r�|j|d< t|fddi|��| _t|fddi|��| _dd� | jjD �| _	dd� | jjD �| _
|| _|| _t|	| j�� �| _t|
| jt| j�|	d |�| _|| _|| _d| _d| _d| _||||||	|
|d�| _dS )ar  
        args:
            network:     Network with a get_losses method
            train_set:   Dataset on which to perform batched gradient descent
            valid_set:   Dataset for validation loss and early stopping conditions
        kwargs:
            b_size:      Batch size to use in the minibatch gradient descent
            patience:    Early stopping patience calculated using the validation set
            max_epochs:  Maximum number of epochs to train for
            grad_clip:   Clip value for the norm of the gradients (0 will not clip)
            n_workers:   Number of parallel threads which prepare each batch
            optim_dict:  A dict used to select and configure the optimiser
            sched_dict:  A dict used to select and configure the scheduler
            vis_on_save: Run a visualisation function on the first batch of the dataset
        z
Initialising the trainer�adamg-C��6?)�name�lrr   �noneztrain set: �7z sampleszvalid set: zNo validation set addedT)�
batch_size�num_workers�	drop_last�
pin_memory�col_fn�
collate_fn�shuffleFc                 S   s   i | ]}|d d� dD ��qS )c                 S   s   i | ]
}|g �qS � r'   )�.0�setr'   r'   �</home/users/l/leighm/neutrinoregression/Resources/trainer.py�
<dictcomp>U   �    z/Trainer.__init__.<locals>.<dictcomp>.<dictcomp>)�train�validr'   �r(   �lsnmr'   r'   r*   r+   T   s   �z$Trainer.__init__.<locals>.<dictcomp>c                 S   s   i | ]}|t � �qS r'   )r   r/   r'   r'   r*   r+   Z   r,   r   r   )r   r   r   r   r   r   r   r   N)�print�network�len�hasattrr$   r   �train_loader�valid_loader�
loss_names�	loss_hist�run_lossr   r   r   �
parameters�	optimiserr   �	schedulerr   r   �
num_epochs�
bad_epochs�
best_epoch�config)�selfr2   r   r   r   r   r   r   r   r   r   r   Zloader_kwargsr'   r'   r*   �__init__   sZ    �

��	�zTrainer.__init__)r   c                 C   sz   t d� t�| j| j�D ]T}t d|� �� | jdd� | jdd� | ��  | ��  | j| j	krt d�  dS qt d� dS )	z�The main loop which cycles epochs of train and test
        - After each epochs it calls the save function and checks for early stopping
        z
Starting the training processz
Epoch: T)�is_trainFz%Patience Exceeded: Stopping training!r   z!Maximum number of epochs exceeded)
r1   �np�aranger=   r   �epoch�count_epochs�save_checkpointr>   r   )rA   Zepcr'   r'   r*   �run_training_loop{   s    zTrainer.run_training_loopF)rC   r   c                 C   s   |r$d}| j ��  | j}t�d� nd}| j ��  | j}t�d� t||dd�D ]�}t|| j j	�}| j j
|� }|r�| j��  |d ��  | jr�tj�| j �� | j� | j��  | jdur�| j��  | j�� D ]\}}|�|| �� � q�qP| j�� D ]&\}}| j| | �|j� |��  q�dS )	a$  Perform a single epoch on either the train loader or the validation loader
        - Will update average loss during epoch
        - Will add average loss to loss history at end of epoch

        kwargs:
            is_train: Effects gradient tracking, network state, and data loader
        r-   Tr.   F�P   )�desc�ncols�totalN)r2   r-   r5   �T�set_grad_enabled�evalr6   r   r	   �device�
get_lossesr;   �	zero_grad�backwardr   �nn�utils�clip_grad_norm_r:   �stepr<   r9   �items�update�itemr8   �append�avg�reset)rA   rC   �mode�loader�sample�losses�lnm�runningr'   r'   r*   rF   �   s2    







zTrainer.epochc                 C   s@   t | jd d �| _t�| jd d �d | _| j| j | _dS )z9Update attributes counting number of bad and total epochsrM   r-   r.   �   N)r3   r8   r=   rD   �argminr?   r>   )rA   r'   r'   r*   rG   �   s    zTrainer.count_epochsc                 C   s~  t d� t| jjd�}|jddd� | j�� | j�� | jd�}| jdurV| j�� |d< t	�
|t|d| j� ��� | jd	kr�t	�
| jt|d
�� t	�
| j�� t|d�� t| jjd�}|jddd� t|d�}t|| j� t|ddd��"}tj| j|dd� W d  � n1 �s0    Y  | j�rz| j��  t	�d� t| jjd�}|jddd� tt| j��}| jj||t| j�d�� dS )a   Add folders to the network's save directory containing
        - models -> Network and the optimiser states
        - losses -> Loss history as json and plotted as pngs
        - vis -> Output of network's visualisation method on first valid batch
        z	Saving...�modelsT)�parents�exist_ok)r2   r;   rb   Nr<   �checkpoint_r   �bestZbest_state_dictrb   zlosses.json�wzutf-8)�encodingr   )�indentFZvisual)�path�flag)r1   r   r2   �	full_name�mkdir�
state_dictr;   r8   r<   rN   �saver=   r>   r
   �open�json�dumpr   rP   rO   �next�iterr6   �	visualise�str)rA   Zmodel_folder�
checkpointZlosses_folderZloss_file_nameZl_fileZ
vis_folderra   r'   r'   r*   rH   �   s4    �


2

zTrainer.save_checkpoint�latestc                 C   s�   t d� |dkrHt| j�D ]*}t| jdd|� ��}|�� r@|}q q\qnt| jdd|� ��}t�|�}| j�	|d � | j
�	|d � |d | _| jdur�| j�	|d	 � | j�� |d	< | ��  dS )
zALoads the latest instance of a saved network to continue trainingzLoading checkpoint...r}   rg   rj   r2   r;   r8   Nr<   )r1   �ranger   r   rq   �is_filerN   �loadr2   �load_state_dictr;   r8   r<   rs   rG   )rA   rp   �iZ	test_fileZcheckpoint_filer|   r'   r'   r*   �load_checkpoint  s     


zTrainer.load_checkpoint)r   r   r   r   r   NNT)F)r}   )�__name__�
__module__�__qualname__�__doc__r   �int�float�dict�boolrB   rI   rF   rG   rH   r�   r'   r'   r'   r*   r      s6           ��e95r   )r�   �pathlibr   rv   �numpyrD   r   �torchrN   �torch.nnrU   �torch.utils.datar   r   �Resources.utilsr   r   r   r	   �Resources.plottingr
   r   r'   r'   r'   r*   �<module>   s   