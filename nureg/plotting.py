"""
A collection of of functions useful for some of the package's plotting code
"""


def nu_plot_name(nu_name: str) -> str:
    """Return a name to use in a plot legend or axis"""
    if nu_name == "MET_Truth":
        return r"$\vec{p}_T^\mathrm{miss}$ + Truth $p_z^\nu$"
    if "MET" in nu_name:
        return r"$\vec{p}_T^\mathrm{miss}+m_W$ Constraint"
    if "NuFlow" in nu_name:
        if "FF" in nu_name:
            return r"$\nu$-FF"
        if "BNuFlow" in nu_name:
            return r"B$\nu$-Flows(mode)"
        if "Mode" in nu_name:
            return r"$\nu$-Flows(mode)"
        return r"$\nu$-Flows(sample)"
    if nu_name == "Truth":
        return "Truth Neutrino"
    return nu_name


def nu_plot_colour(nu_name: str) -> str:
    """Return a colour to use for matplotlib"""
    if nu_name == "MET_Quad":
        return "C2"
    if nu_name == "MET_Truth":
        return "green"
    if nu_name in ["Idealised", "Truth"]:
        return "k"
    if "NuFlow" in nu_name:
        if "FF" in nu_name:
            return "C0"
        if "Mode" in nu_name:
            return "C3"
        if "BNuFlow" in nu_name:
            return "C4"
        return "C1"
    raise ValueError(f"Cant find colour for {nu_name}")


def nu_plot_ls(nu_name: str) -> str:
    """Return a linestyle given a neutrino type name"""
    if nu_name in ["Idealised", "Truth"]:
        return "--"
    return "-"
