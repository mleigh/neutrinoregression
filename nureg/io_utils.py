from pathlib import Path
from typing import Optional, Tuple
import h5py
import numpy as np
import numpy.lib.recfunctions as rf
from dotmap import DotMap

from nureg.physics import Mom4Vec, nu_sol_comps, combine_comps


def read_net(
    export_dir: str, network_name: str, njet_sel: np.ndarray = None, mask: np.ndarray = None
) -> Tuple[Mom4Vec, np.ndarray]:
    """Read the outputs of a network and return the stored momentum and log probs"""

    ## Load the file containing the network outputs (load only njet sel evnts)
    with h5py.File(Path(export_dir) / network_name, "r") as f:
        net_nu = f["gen_neutrinos"][:]
        net_logp = f["log_probs"][:]

    ## Convert to a cartisian momentum 4 vector
    net_nu = Mom4Vec(net_nu, is_cartesian=True, n_mom=3)
    net_nu.to_cartesian()

    return net_nu, net_logp


def get_neutrinos(
    nu: str,
    tru_nu: Mom4Vec,
    met: Mom4Vec,
    export_dir: str
) -> Mom4Vec:
    """Return an Mom4Vec object containing neutrinos specified by the nu string"""
    nu_type = nu.split("_")

    ## The network neutrinos
    if "NuFlow" in nu:

        ## Need to be loaded from file
        net_nu, net_logp = read_net(export_dir, "_".join(nu_type[:-1]))

        ## If using the node we split everything off and select only highest logp
        if nu_type[-1] == "Mode":
            idxs = np.arange(len(net_nu.mom))
            data = Mom4Vec(net_nu.mom[idxs, np.newaxis, np.argmax(net_logp, axis=-1)])
            net_logp = np.max(net_logp, axis=-1)
        else:
            data = Mom4Vec(net_nu.mom[:, 0 : int(nu_type[-1])])

        ## Combining with truth for the final dimension
        if nu_type[-1] == "Truth":
            data = Mom4Vec(
                np.concatenate(
                    [
                        data.mom[..., :2],
                        np.repeat(tru_nu.mom[..., 2:3], data.mom.shape[-2], -2),
                    ],
                    axis=-1,
                ),
                n_mom=3,
            )

        ## Combining with quad for the final dimension
        if nu_type[-1] == "Quad":
            data = Mom4Vec(
                np.concatenate(
                    [
                        np.repeat(data.mom[..., :2], 2, -2),
                        np.tile(met.mom[..., 2:3], (1, data.mom.shape[-2], 1)),
                    ],
                    axis=-1,
                ),
                n_mom=3,
            )

        return data, net_logp

    ## The MET based neutrinos (already contain quad solutions)
    log_p = np.zeros(len(tru_nu))
    if nu_type[0] == "MET":
        if nu_type[1] == "Quad":
            return met, log_p
        elif nu_type[1] == "Truth":
            return (
                Mom4Vec(
                    np.concatenate([met.mom[:, :1, :2], tru_nu.mom[..., 2:3]], axis=-1),
                    n_mom=3,
                ),
                log_p,
            )

    ## For truth neutrinos
    if nu_type[0] == "Truth":
        return tru_nu, log_p

    raise ValueError(f"Dont know what to do with neutrino type: {nu}")


def read_singlelep_file(file_path: str, max_jets: Optional[int] = None) -> DotMap:
    """Reads in data from a single HDF file, returning the collection of information
    as a DotMap object"""

    with h5py.File(file_path, "r") as f:
        ## Read in the particle tensors
        met = rf.structured_to_unstructured(f["delphes"]["MET"][:])
        leptons = rf.structured_to_unstructured(f["delphes"]["leptons"][:])
        jets = rf.structured_to_unstructured(f["delphes"]["jets"][:])

        ## Read in the truth information (which may or may not be available!)
        if "neutrinos" in f["delphes"].keys():
            tru_nu = rf.structured_to_unstructured(f["delphes"]["neutrinos"][:])
        else:
            tru_nu = None

        if "jets_indices" in f["delphes"].keys():
            indices = f["delphes"]["jets_indices"][:]
        else:
            indices = None

        if "matchability" in f["delphes"].keys():
            match = f["delphes"]["matchability"][:]
        else:
            match = None

    ## Drop neutrino PDGID and add extra dimension (for nu options)
    tru_nu = np.expand_dims(tru_nu[..., 1:], -2)

    ## Store the number of jets in the event, THEN clamp
    njets = np.sum(jets[..., 0] > 0, axis=-1).astype(np.int64)
    jets = jets[:, :max_jets]
    indices = indices[:, :max_jets]

    ## Store the important indicies of the jets in the file
    jet_idx = important_indices(indices)

    ## Change the jet padding to nans so they wont show up in min chi square!
    jets[jets[..., 0] == 0] *= np.nan

    ## Change some of the entries to 4 vectors
    met = Mom4Vec(met, is_cartesian=False, n_mom=2)
    tru_nu = Mom4Vec(tru_nu, is_cartesian=False, n_mom=3)
    leptons = Mom4Vec(leptons, is_cartesian=False)
    jets = Mom4Vec(jets, is_cartesian=False)
    met.to_cartesian()
    tru_nu.to_cartesian()
    leptons.to_cartesian()
    jets.to_cartesian()

    ## Add the quadratic solutions to the met tensor
    met = add_quad_to_met(met, leptons)

    ## Return all information as a dictionary
    data = DotMap(
        {
            "met": met,
            "leptons": leptons,
            "jets": jets,
            "tru_nu": tru_nu,
            "indices": indices,
            "match": match,
            "njets": njets,
            "jet_idx": jet_idx,
        }
    )
    return data


def important_indices(indices: np.ndarray, nf_value: int = 9999) -> np.ndarray:
    """Returns the indices of the blep, bhad, q1, q2 in the jet data table

    If these particular jets are NOT in the jet data table, then the returned index
    takes the nf_value

    args:
        indices: The jets_indices table, of shape (N,10)
    returns:
        jet_idx: The locations of the important jets in the table, shape (N,4)
    """

    ## Check which jet in the jet table belongs to which type
    is_blep = indices == 0
    is_bhad = indices == 1
    is_w_q1 = indices == 2
    is_w_q2 = indices == 3

    ## Check if the event has these types
    has_blep = np.any(is_blep, axis=1)
    has_bhad = np.any(is_bhad, axis=1)
    has_w_q1 = np.any(is_w_q1, axis=1)
    has_w_q2 = np.any(is_w_q2, axis=1)

    ## If the event has the jet, return its index in the jet table, otherwise return nf
    blep_idx = np.where(has_blep, np.argmax(is_blep, axis=1), nf_value)
    bhad_idx = np.where(has_bhad, np.argmax(is_bhad, axis=1), nf_value)
    w_q1_idx = np.where(has_w_q1, np.argmax(is_w_q1, axis=1), nf_value)
    w_q2_idx = np.where(has_w_q2, np.argmax(is_w_q2, axis=1), nf_value)

    ## Define the jet indices as a single tensor (order the W's so q1 comes first)
    jet_idx = np.vstack(
        [
            blep_idx,
            bhad_idx,
            np.minimum(w_q1_idx, w_q2_idx),
            np.maximum(w_q1_idx, w_q2_idx),
        ]
    ).T

    return jet_idx


def evt_selection(sel: str, match: np.ndarray) -> np.ndarray:
    """Return a mask for using the matchability array and a given selection"""

    ## Check the selection string validity
    if sel not in ["none", "blep", "all_jets"]:
        raise ValueError(f"Unrecognized event selection option: {sel}")

    ## Apply the appropriate selection and return the masking tensor
    if sel == "none":
        return np.ones_like(match, dtype=bool)
    if sel == "blep":
        return ((match>>3) & 1).astype(bool)
    if sel == "all_jets":
        return match == 15


def get_quality_metric(nu_data: DotMap, nu_cut: Optional[str] = None) -> np.ndarray:
    """Return a quality metric given the neutrino values"""

    ## Split the three components of the cut
    cut_nu, cut_type, _ = nu_cut.split(":")

    ## Pull out the specific data from the neutrino tuples
    nus, logps = nu_data[cut_nu]

    ## Use the negative value
    fac = 1
    if cut_type[0] == "-":
        cut_type = cut_type[1:]
        fac = -1

    ## Get the metric from the cut type
    if cut_type == "logp":
        metric = logps.squeeze() * fac
    if cut_type == "sigz":
        metric = nus.pz.std(axis=1).squeeze() * fac
    if cut_type == "GMM":
        metric = gmm_fits(nus, logps)

    return metric

def get_quality_mask(nu_data: DotMap, nu_cut: Optional[str] = None) -> np.ndarray:
    """Create a mask for the neutrino quality based on the flow's output"""


    ## Return all true's if the metric is set to all
    if nu_cut == "all":
        return np.full(True, len(nu_data[0]))

    ## Get the metric values for each event
    metric = get_quality_metric(nu_data, nu_cut)

    ## Split the three components of the cut
    _, _, cut_eff = nu_cut.split(":")

    ## Return a mask based of some quantile cut
    return metric > np.quantile(metric, 1 - float(cut_eff))


def add_quad_to_met(met: Mom4Vec, leptons: Mom4Vec) -> Mom4Vec:
    """Add the quadratic solutions to the met 4 vector"""
    sol_1 = combine_comps(
        *nu_sol_comps(
            leptons.px,
            leptons.py,
            leptons.pz,
            leptons.energy,
            leptons.oth[..., -1:],  ## lepton ID type is stored in final variable
            met.px,
            met.py,
        ),
        return_both=False,
    )
    met.mom = np.expand_dims(met.mom, -2)
    met.oth = np.expand_dims(met.oth, -2)
    # met.mom = np.repeat(met.mom, 2, -2)
    # met.oth = np.repeat(met.oth, 2, -2)
    met.mom[:, 0, 2:3] = sol_1
    # met.mom[:, 1, 2:3] = sol_2
    met.set_massless_energy()
    return met


def read_and_mask(
    file_path: str,
    export_dir: str,
    neutrino_list: list,
    max_jets: int,
    evt_sel: str,
    nu_cut: Optional[str] = None,
) -> Tuple[DotMap, DotMap, DotMap]:
    """Read in data from the HDF file and mask based on the max number of jets
    and a particular event selection using the matchability variables
    """

    ## Read in all of the important data from the hdf file
    data = read_singlelep_file(file_path, max_jets)

    ## Read in the different neutrino information, store as tuples
    nu_data = DotMap(
        {nu: get_neutrinos(nu, data.tru_nu, data.met, export_dir) for nu in neutrino_list}
    )

    ## Get the mask by combining the number of jets and matchability
    mask = evt_selection(evt_sel, data.match) & (data.njets <= max_jets)
    if nu_cut is not None and nu_cut != "none":
        mask = mask & get_quality_mask(nu_cut)

    ## Apply the mask
    data.update({k: v[mask] for k, v in data.items()})
    nu_data.update({k: (mom[mask], logp[mask]) for k, (mom, logp) in nu_data.items()})

    return data, nu_data, mask
