"""
A collection of functions for particle physics calulations
"""
from __future__ import annotations
from typing import Any, Tuple, Union
import numpy as np
import torch as T

from mattstools.torch_utils import empty_0dim_like


class Mom4Vec:
    """A simple class that contains the four vectors of events in either
    px, py, pz, E or pt, eta, phi, E

    Will automatically work with both numpy arrays and pytorch tensors

    """

    def __init__(
        self,
        data: Union[np.ndarray, T.Tensor],
        oth: Union[None, np.ndarray, T.Tensor] = None,
        is_cartesian: bool = True,
        n_mom: int = 4,
    ):
        """
        args:
            data: The input array where the last dimension points to the coordinates
            oth: Other data which is not used as part of the momentum
        kwargs:
            is_cartesian: If the data provided is wrt cartesian coordinates
            n_mom: How many variables in the list refer to momentum
        """

        ## Save the attributes
        self.is_cartesian = is_cartesian
        if isinstance(data, T.Tensor):
            self.is_tensor = True
        elif isinstance(data, np.ndarray):
            self.is_tensor = False
        else:
            raise ValueError(
                "Mom4Vec is not able to tell if data is a torch tensor "
                "or a numpy array!"
            )

        ## Create the 4 momentum array/tensor
        if self.is_tensor:
            self.mom = T.zeros((*data.shape[:-1], 4), dtype=T.float32)
        else:
            self.mom = np.zeros((*data.shape[:-1], 4), dtype=np.float32)

        ## Fill in the array
        if n_mom == 4:
            self.mom = data[..., :4]
        elif n_mom == 3:
            self.mom[..., :3] = data[..., :3]
            self.set_massless_energy()
        elif n_mom == 2:
            if self.is_cartesian:
                self.mom[..., :2] = data[..., :2]
                self.set_massless_energy()
            else:
                self.mom[..., 0:1] = data[..., 0:1]
                self.mom[..., 2:3] = data[..., 1:2]
                self.set_massless_energy()

        ## The leftover tensor which holds the rest of the variables
        if oth is None:
            self.oth = data[..., n_mom:]
        else:
            if self.is_tensor:
                self.oth = T.cat([data[..., n_mom:], oth], dim=-1)
            else:
                self.oth = np.concatenate([data[..., n_mom:], oth], axis=-1)

    @property
    def shape(self):
        return self.mom.shape

    @property
    def pt(self) -> Union[np.ndarray, T.Tensor]:
        """Return the transverse momentum"""
        if self.is_cartesian:
            pt = np.linalg.norm(self.mom[..., :2], axis=-1, keepdims=True)
            if self.is_tensor:
                pt = T.from_numpy(pt)
            return pt
        else:
            return self.mom[..., 0:1]

    @property
    def eta(self) -> Union[np.ndarray, T.Tensor]:
        """Return the pseudorapitity"""
        if self.is_cartesian:
            return np.arctanh(
                np.clip((self.pz / (self.p3_mag + 1e-8)), 1e-6 - 1, 1 - 1e-6)
            )
        else:
            return self.mom[..., 1:2]

    @property
    def phi(self) -> Union[np.ndarray, T.Tensor]:
        """Return the polar angle"""
        if self.is_cartesian:
            return np.arctan2(self.py, self.px)
        else:
            return self.mom[..., 2:3]

    @property
    def px(self) -> Union[np.ndarray, T.Tensor]:
        """Return the momentum x component"""
        if self.is_cartesian:
            return self.mom[..., 0:1]
        else:
            return self.pt * np.cos(self.phi)

    @property
    def py(self) -> Union[np.ndarray, T.Tensor]:
        """Return the momentum y component"""
        if self.is_cartesian:
            return self.mom[..., 1:2]
        else:
            return self.pt * np.sin(self.phi)

    @property
    def pz(self) -> Union[np.ndarray, T.Tensor]:
        """Return the momentum z component"""
        if self.is_cartesian:
            return self.mom[..., 2:3]
        else:
            return self.pt * np.sinh(self.eta)

    @property
    def p3_mag(self) -> Union[np.ndarray, T.Tensor]:
        """Return the magnitude of the 3 momentum"""
        if self.is_cartesian:
            p3_mag = np.linalg.norm(self.mom[..., :3], axis=-1, keepdims=True)
            if self.is_tensor:
                p3_mag = T.from_numpy(p3_mag)
            return p3_mag
        else:
            return self.pt * np.cosh(self.eta)

    @property
    def energy(self) -> Union[np.ndarray, T.Tensor]:
        """Return the energy"""
        return self.mom[..., 3:4]

    @property
    def mass(self) -> Union[np.ndarray, T.Tensor]:
        """Return the mass using the m2=E2-p2"""
        if self.is_cartesian:
            return np.sqrt(
                np.abs(self.energy**2 - (self.px**2 + self.py**2 + self.pz**2))
            )
        else:
            mass = np.sqrt(
                np.abs(self.energy**2 - (self.pt * np.cosh(self.eta)) ** 2)
            )
            return mass

    def __len__(self):
        return len(self.mom)

    def apply_mask(self, mask: np.ndarray) -> None:
        """Apply a mask to both the momentum and the other arrays"""
        self.mom = self.mom[mask]
        self.oth = self.oth[mask]

    def set_massless_energy(self) -> None:
        """Sets the energy part of the 4 momentum tensor to be the 3 momentum mag"""
        self.mom[..., 3:4] = self.p3_mag

    def to_cartesian(self) -> None:
        """Changes the saved momentum tensor to cartesian inplace"""
        if self.is_cartesian:
            return

        else:
            px = self.px.clone() if self.is_tensor else self.px.copy()
            py = self.py.clone() if self.is_tensor else self.py.copy()
            pz = self.pz.clone() if self.is_tensor else self.pz.copy()
            self.mom[..., 0:1] = px
            self.mom[..., 1:2] = py
            self.mom[..., 2:3] = pz
            self.is_cartesian = True

    def to_spherical(self) -> None:
        """Changes the saved momentum tensor to cartesian inplace"""
        if not self.is_cartesian:
            return

        else:
            pt = self.pt.clone() if self.is_tensor else self.pt.copy()
            eta = self.eta.clone() if self.is_tensor else self.eta.copy()
            phi = self.phi.clone() if self.is_tensor else self.phi.copy()
            self.mom[..., 0:1] = pt
            self.mom[..., 1:2] = eta
            self.mom[..., 2:3] = phi
            self.is_cartesian = False

    def __add__(self, other: Mom4Vec) -> Mom4Vec:
        """Add two collections of four momentum together"""
        assert self.is_cartesian and other.is_cartesian
        return Mom4Vec(self.mom + other.mom, is_cartesian=True)

    def __getitem__(self, idx: Union[int, np.ndarray, slice]) -> Mom4Vec:
        """Index, mask or slice the object"""
        if isinstance(idx, int):
            if idx == -1:
                idx = slice(idx, None)
            else:
                idx = slice(idx, idx + 1)
        return Mom4Vec(self.mom[idx], self.oth[idx], is_cartesian=self.is_cartesian)


def change_cords(
    data: T.tensor, old_cords: list, new_cords: list
) -> Tuple[T.Tensor, list]:
    """Converts a tensor from spherical/to cartesian coordinates

    args:
        data: A multidimensional tensor containin the sperical components
        old_names: The current names of the coords
        new_names: The new coords to calculate
    returns:
        new_values, new_names

    Makes assumptions on based on number of features in final dimension
    - 2D = pt, phi
    - 3D = pt, eta, phi
    - 4D = pt, eta, phi, energy
    - 4D+ = pt, eta, phi, energy, other (not changed, always kept)

    """

    ## Allow a string to be given which can be seperated into a list
    old_names = old_cords.split(",") if isinstance(old_cords, str) else old_cords
    new_names = new_cords.split(",") if isinstance(new_cords, str) else new_cords

    ## List of supported new names
    for new_nm in new_names:
        if new_nm not in [
            "pt",
            "log_pt",
            "energy",
            "log_energy",
            "phi",
            "cos",
            "sin",
            "eta",
            "px",
            "py",
            "pz",
        ]:
            raise ValueError(f"Unknown coordinate name: {new_nm}")

    ## Calculate the number of features in the final dimension
    n_dim = data.shape[-1]

    ## Create the slices
    pt = data[..., 0:1]
    eta = data[..., 1:2] if n_dim > 2 else empty_0dim_like(data)
    phi = data[..., 2:3] if n_dim > 2 else data[..., 1:2]
    eng = data[..., 3:4] if n_dim > 3 else empty_0dim_like(data)
    oth = data[..., 4:] if n_dim > 4 else empty_0dim_like(data)

    ## If energy is empty then we might try use pt and eta
    if eng.shape[-1] == 0 and eta.shape[-1] > 0:
        eng = pt * T.cosh(eta)

    ## A dictionary for calculating the supported new variables
    ## Using lambda functions like this prevents execution every time
    new_coord_fns = {
        "pt": lambda: pt,
        "log_pt": lambda: pt.log(),
        "energy": lambda: eng,
        "log_energy": lambda: eng.log(),
        "phi": lambda: phi,
        "cos": lambda: T.cos(phi),
        "sin": lambda: T.sin(phi),
        "eta": lambda: eta,
        "px": lambda: pt * T.cos(phi),
        "py": lambda: pt * T.sin(phi),
        "pz": lambda: pt * T.sinh(eta),
        "oth": lambda: oth,
    }

    ## Create a dictionary of the requested coordinates then trim non empty values
    new_coords = {key: new_coord_fns[key]() for key in new_names}
    new_coords = {key: val for key, val in new_coords.items() if val.shape[-1] != 0}

    ## Return the combined tensors and the collection of new names (with unchanged)
    new_vals = T.cat(list(new_coords.values()) + [oth], dim=-1)
    new_names = list(new_coords.keys())
    new_names += old_names[4:]

    return new_vals, new_names


def neut_to_pxpypz(data: T.Tensor, names: list, to_pxpyeta: bool = False) -> T.Tensor:
    """Convert a tensor containing neutrino information back to px, py, pz

    This is primarily for evaluating a model which is trained using any collection
    of inputs as we need a standard set

    args:
        data: The data tensor where each row represents a single neutrino
        names: The list of variables contained in final dimension of data
    kwargs:
        to_pxpyeta: If the returned tensor should have eta in the final dimension not z
    """

    ## List of supported new names
    for new_nm in names:
        if new_nm not in [
            "pt",
            "log_pt",
            "energy",
            "log_energy",
            "phi",
            "cos",
            "sin",
            "eta",
            "px",
            "py",
            "pz",
        ]:
            raise ValueError(f"Unknown coordinate name: {new_nm}")

    ## Create a dict of the co_ordinates we have access to
    cdt = {nm: data[..., names.index(nm)] for nm in names}

    ## First we need to calculate pt (used for eta)
    if "pt" in cdt.keys():
        pass
    elif "log_pt" in cdt.keys():
        cdt["pt"] = cdt["log_pt"].exp()
    elif "px" in cdt.keys() and "py" in cdt.keys():
        cdt["pt"] = T.sqrt(cdt["px"] ** 2 + cdt["py"] ** 2)
    else:
        raise ValueError(f"Can't calculate pt given neutrino variables: {names}")

    ## Calculate px
    if "px" in cdt.keys():
        pass
    elif "cos" in cdt.keys():
        cdt["px"] = cdt["pt"] * cdt["cos"]
    elif "phi" in cdt.keys():
        cdt["px"] = cdt["pt"] * T.cos(cdt["phi"])
    else:
        raise ValueError(f"Can't calculate px given neutrino variables: {names}")

    ## Calculate py
    if "py" in cdt.keys():
        pass
    elif "sin" in cdt.keys():
        cdt["py"] = cdt["pt"] * cdt["sin"]
    elif "phi" in cdt.keys():
        cdt["py"] = cdt["pt"] * T.sin(cdt["phi"])
    else:
        raise ValueError(f"Can't calculate py given neutrino variables: {names}")

    ## Calculate eta
    if to_pxpyeta:
        if "eta" in cdt.keys():
            pass
        elif "pz" in cdt.keys():
            cdt["eta"] = T.arctanh(
                cdt["pz"] / T.sqrt(1e-8 + cdt["pz"] ** 2 + cdt["pt"] ** 2)
            )
        else:
            raise ValueError(f"Can't calculate eta given neutrino variables: {names}")

    ## Calculate pz instead of eta
    else:
        if "pz" in cdt.keys():
            pass
        elif "eta" in cdt.keys():
            cdt["pz"] = cdt["pt"] * T.sinh(cdt["eta"])
        else:
            raise ValueError(f"Can't calculate pz given neutrino variables: {names}")

    ## Combine the final measurements
    return T.cat(
        [
            cdt["px"].unsqueeze(-1),
            cdt["py"].unsqueeze(-1),
            cdt["pz" if not to_pxpyeta else "eta"].unsqueeze(-1),
        ],
        dim=-1,
    )


def nu_sol_comps(
    lept_px: np.ndarray,
    lept_py: np.ndarray,
    lept_pz: np.ndarray,
    lept_e: np.ndarray,
    lept_ismu: np.ndarray,
    nu_px: np.ndarray,
    nu_py: np.ndarray,
) -> np.ndarray:
    """Calculate the components of the quadratic solution for neutrino pseudorapidity"""

    ## Constants NuRegData is in GeV!
    w_mass = 80379 * 1e-3
    e_mass = 0.511 * 1e-3
    mu_mass = 105.658 * 1e-3

    ## Create an array of the masses using lepton ID
    l_mass = np.where(lept_ismu != 0, mu_mass, e_mass)

    ## Calculate all components in the quadratic equation
    nu_ptsq = nu_px**2 + nu_py**2
    alpha = w_mass**2 - l_mass**2 + 2 * (lept_px * nu_px + lept_py * nu_py)
    a = lept_pz**2 - lept_e**2
    b = alpha * lept_pz
    ## c = alpha**2 / 4 - lept_e**2 * nu_ptsq ## Using delta instead (quicker)
    delta = lept_e**2 * (alpha**2 + 4 * a * nu_ptsq)

    comp_1 = -b / (2 * a)
    comp_2 = delta / (4 * a**2)

    ## Take the sign preserving sqrt of comp_2 due to scale
    comp_2 = np.sign(comp_2) * np.sqrt(np.abs(comp_2))
    return comp_1, comp_2


def combine_comps(
    comp_1: np.ndarray,
    comp_2: np.ndarray,
    return_eta: bool = False,
    nu_pt: np.ndarray = None,
    return_both: bool = False,
) -> np.ndarray:
    """Combine the quadiratic solutions and pick one depending on complexity and size
    args:
        comp_1: First component of the quadratic
        comp_2: Signed root of the second component of the quadratic
    kwargs:
        return_eta: If the output should be eta, otherwise pz
        nu_pt: The neutrino pt, needed only if return_eta is true
        return_both: Return both solutions
    """

    ## comp_2 is already rooted, so the real component is taken to be 0 if negative
    comp_2_real = np.where(comp_2 > 0, comp_2, np.zeros_like(comp_2))

    ## Get the two solutions
    sol_1 = comp_1 + comp_2_real
    sol_2 = comp_1 - comp_2_real

    ## If both solutions are requested
    if return_both:
        if return_eta:
            return (
                np.arctanh(sol_1 / np.sqrt(sol_1**2 + nu_pt**2 + 1e-8)),
                np.arctanh(sol_2 / np.sqrt(sol_2**2 + nu_pt**2 + 1e-8)),
            )
        return sol_1, sol_2

    ## Take the smallest solution based on magnitude
    sol = np.where(np.abs(sol_1) < np.abs(sol_2), sol_1, sol_2)

    ## Return correct variable
    if return_eta:
        return np.arctanh(sol / np.sqrt(sol**2 + nu_pt**2 + 1e-8))
    else:
        return sol
