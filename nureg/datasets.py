"""
Pytorch Dataset definitions of the neutrino regression samples
"""

from copy import deepcopy
from pathlib import Path
from typing import Tuple
from joblib import dump
import h5py

import torch as T
from torch.utils.data import Dataset

from mattstools.plotting import plot_multi_hists
from mattstools.utils import get_scaler, signed_angle_diff
from mattstools.torch_utils import to_np

from nureg.physics import change_cords, nu_sol_comps, Mom4Vec


def argminmax_2d(tensor: T.Tensor, minmax: str) -> Tuple[T.IntTensor, T.IntTensor]:
    """Returns the row and column indices of the minimum per sample of a 3D tensor"""

    ## Get the argmin or max of the flattened tensor
    if minmax == "min":
        flat_index = T.argmin(tensor.view(len(tensor), -1), dim=-1)
    elif minmax == "max":
        flat_index = T.argmax(tensor.view(len(tensor), -1), dim=-1)
    else:
        raise ValueError(f"Unknown minmax option: {minmax}")

    ## Unravel the indices that were returned
    x_index = T.div(flat_index, tensor.shape[-1], rounding_mode="floor")
    y_index = flat_index % tensor.shape[-1]

    return x_index, y_index


def get_delr_matrix(
    eta_1: T.Tensor, phi_1: T.Tensor, eta_2: T.Tensor = None, phi_2: T.Tensor = None
) -> T.Tensor:
    """Build a delta R matrix given the eta and phi tensors"""

    if eta_2 is None:
        eta_2 = eta_1
    if phi_2 is None:
        phi_2 = phi_1

    del_eta = eta_1.unsqueeze(-1) - eta_2.unsqueeze(-2)
    del_phi = signed_angle_diff(phi_1.unsqueeze(-1), phi_2.unsqueeze(-2))
    return (del_eta**2 + del_phi**2).sqrt()


def pair_kinematics(jets: T.Tensor, del_r: T.Tensor, minmax: str):
    """Return the kinematics of the either the closest or furthest seperated jets"""
    indx = T.arange(len(jets))

    ## Get the indices of the pair
    j1_indx, j2_indx = argminmax_2d(del_r, minmax)

    ## Use the Mom4Vec object for efficient inv system calculations
    j_1 = Mom4Vec(jets[indx, j1_indx], is_cartesian=False)
    j_2 = Mom4Vec(jets[indx, j2_indx], is_cartesian=False)
    j_1.to_cartesian()
    j_2.to_cartesian()
    jet_pair = Mom4Vec(j_1.mom + j_2.mom)
    jet_pair.to_spherical()

    ## Return the momenta and the log of the mass
    return T.hstack([jet_pair.mom, jet_pair.mass.log()])


def build_jet_pairs(
    jets: T.Tensor, leptons: T.Tensor, jet_vars: list
) -> Tuple[T.Tensor, list]:
    """Create handcrafted jet information using pairs of jets and the leptons

    Handcrafted information is the:
    - Kinematics or the closest two jets
    - Kinematics of the furthest two jets
    - Seperation of the furthest two jets
    - Kinematics of the lepton and closest jet

    args:
        jets: Tensor of shape data, njets, (pt, eta, phi, energy, ...)
        leptons Tensor of shape data, (pt, eta, phi, energy, ...)
        jet_vars: The variables to use to describe the jet kinematics
    """

    ## Start by trimming to the leading 4 jets in the event
    jet_kins = jets[:, :4, :4]
    indx = T.arange(len(jet_kins))

    ## Create a delta R matrix of the jets
    del_r = get_delr_matrix(jet_kins[..., 1], jet_kins[..., 2])

    ## Get the kinematics of the invariant system of closest two jets and furthest
    del_r[del_r == 0] = T.inf
    min_jet_pair = pair_kinematics(jet_kins, del_r, "min")
    del_r[del_r == T.inf] = -T.inf
    max_jet_pair = pair_kinematics(jet_kins, del_r, "max")

    ## Also get the dR of the max jet pair
    i_1, i_2 = argminmax_2d(del_r, "max")
    max_del_r = del_r[indx, i_1, i_2]

    ## Now get the del_R matrix to the leptons
    del_r_jl = get_delr_matrix(
        jet_kins[..., 1],
        jet_kins[..., 2],
        leptons[..., 1].unsqueeze(-1),
        leptons[..., 2].unsqueeze(-1),
    )
    jl_idx = T.argmin(del_r_jl.squeeze(), dim=-1)
    jl_jet = jet_kins[indx, jl_idx]

    ## Convert to the same vars used in the rest of the jet setup
    min_jet_pair, min_jet_pair_names = change_cords(
        min_jet_pair, ["pt", "eta", "phi", "energy", "log_mass"], jet_vars
    )
    max_jet_pair, max_jet_pair_names = change_cords(
        max_jet_pair, ["pt", "eta", "phi", "energy", "log_mass"], jet_vars
    )
    jl_jet, jl_jet_names = change_cords(
        jl_jet, ["pt", "eta", "phi", "energy"], jet_vars
    )

    ## Combine all he data together
    jet_pairs = T.hstack([min_jet_pair, max_jet_pair, max_del_r.unsqueeze(-1), jl_jet])

    ## Combine the names together
    var_names = (
        [f"minJJ_{v}" for v in min_jet_pair_names]
        + [f"maxJJ_{v}" for v in max_jet_pair_names]
        + ["max_jet_pair_delR"]
        + [f"Jlep_{v}" for v in jl_jet_names]
    )

    return jet_pairs, var_names


class NuRegData(Dataset):
    """Dataset class for neutrino regression with ttbar events"""

    def __init__(
        self,
        dset: str = "train",
        path: str = "SetMePlease",
        lep_vars: str = "log_pt,cos,sin,eta,log_energy",
        jet_vars: str = "log_pt,cos,sin,eta,log_energy",
        out_vars: str = "log_pt,phi,eta",
        scaler_nm: str = "standard",
        variables: dict = None,
        incl_quad: bool = True,
        jet_pairs_in_misc: bool = False,
        n_files: int = None,
    ):
        """
        kwargs:
            dset: Which dataset to pull from, either train, test or valid
            path: The location of the datafiles
            incl_quad: If the terms of the nu_z solution should be included in misc
            lep_vars: The variables to use for the lepton kinematics (also for MET)
            jet_vars: The variables to use for the jet kinematics
            out_vars: The variables to use for the momentum outputs
            scaler_nm: The scaler name for pre-post processing the data
            variables: A dictionary showing which datasets and variables to load
            jet_pairs_in_misc: If the misc data should include selected jet pair data
            n_files: The number of training files to load (-1 is all)
        """
        super().__init__()

        print(f"\nCreating dataset: {dset}")

        ## Save attributes
        self.scaler_nm = scaler_nm

        ## Change the vars to lists and save as class attributes
        self.lep_vars = lep_vars.split(",") if isinstance(lep_vars, str) else lep_vars
        self.jet_vars = jet_vars.split(",") if isinstance(jet_vars, str) else jet_vars
        self.out_vars = out_vars.split(",") if isinstance(out_vars, str) else out_vars

        ## Make sure the dset variable is correct
        if dset not in ["test", "train"]:
            raise ValueError("Unknown dset type: ", dset)

        ## Get the list of files to use for the dataset (0 sample is reserved for test)
        if dset == "train":
            file_list = list(Path(path).glob("events_*_selected*"))[:n_files]
            file_list = [f for f in file_list if "0_selected" not in str(f)]
        else:
            file_list = [Path(path) / "events_0_selected.h5"]

        ## Raise an error if there are no files found
        if not file_list:
            raise ValueError("No files found using path: " + path)

        ## Get the group variables, dict must be copied to prevent overwite
        if variables:
            self.variables = deepcopy(variables)
        else:
            self.variables = {
                "MET": ["MET", "phi"],
                "leptons": ["pt", "eta", "phi", "energy", "type"],
                "misc": ["njets", "nbjets"],
                "jets": ["pt", "eta", "phi", "energy", "is_tagged"],
                "neutrinos": ["pt", "eta", "phi"],
            }

        ## The data starts out as empty tensors
        self.data = {key: [] for key in self.variables}

        ## Cycle through the file list and fill up group data with lists
        print(" - loading file hdf files")
        for file in file_list:
            print(file)

            with h5py.File(file, "r") as f:
                table = f["delphes"]

                ## Fill the group_data dictionary using the group_names to get variables
                for dataset in self.data.keys():

                    ## The misc data is actually a collection of datasets
                    if dataset == "misc":
                        new_lists = [table[ds] for ds in self.variables[dataset]]
                        self.data[dataset] += map(list, zip(*new_lists))

                    ## The jets indices and match is not a names array
                    elif dataset in ["jets_indices", "matchability", "decay_channel"]:
                        self.data[dataset] += table[dataset][:].tolist()

                    ## The rest is a named tuple and we pull out specific columns
                    else:
                        self.data[dataset] += table[dataset][
                            tuple(self.variables[dataset])
                        ].tolist()

        ## Cycle through the group data and turn into pytorch tensors
        for key in self.data.keys():
            self.data[key] = T.tensor(self.data[key], dtype=T.float32)

        ## Add the jet_pair data into the misc array
        if jet_pairs_in_misc:
            print(" - Adding jet pair kinematics to misc tensor")
            jet_pairs, var_names = build_jet_pairs(
                self.data["jets"], self.data["leptons"], self.jet_vars
            )
            self.data["misc"] = T.hstack([self.data["misc"], jet_pairs])
            self.variables["misc"] += var_names

        ## Calculate the jet mask based on existing pt
        self.jet_mask = self.data["jets"][..., 0] > 0

        ## Calculate the components of the solutions for the neutrino quadratic
        ## This is done right away so we can still be flexible in coordinates later
        print(" - calculating quadratic solutions")
        self.comp_1, self.comp_2 = nu_sol_comps(
            to_np(self.data["leptons"][:, 0] * T.cos(self.data["leptons"][:, 2])),
            to_np(self.data["leptons"][:, 0] * T.sin(self.data["leptons"][:, 2])),
            to_np(self.data["leptons"][:, 0] * T.sinh(self.data["leptons"][:, 1])),
            to_np(self.data["leptons"][:, 3]),
            to_np(self.data["leptons"][:, 4]),
            to_np(self.data["MET"][:, 0] * T.cos(self.data["MET"][:, 1])),
            to_np(self.data["MET"][:, 0] * T.sin(self.data["MET"][:, 1])),
        )
        self.comp_1 = T.from_numpy(self.comp_1)
        self.comp_2 = T.from_numpy(self.comp_2)

        ## Add the quadratic components to the misc data
        if incl_quad:
            self.data["misc"] = T.hstack(
                [
                    self.data["misc"],
                    self.comp_1.unsqueeze(-1),
                    self.comp_2.unsqueeze(-1),
                ]
            )
            self.variables["misc"] += ["comp_1", "comp_2"]

        ## Convert to specified coordinates
        print(" - converting variable coordinates")
        for key in self.data:

            ## Get the new variable names based on type
            if key in ["leptons", "MET", "partons"]:
                new_vars = self.lep_vars
            elif key == "jets":
                new_vars = self.jet_vars
            elif key == "neutrinos":
                new_vars = self.out_vars
            else:
                continue

            ## Change the group data tensors and the names
            self.data[key], self.variables[key] = change_cords(
                self.data[key],
                self.variables[key],
                new_vars,
            )

            ## Make sure that the jets still adhere to a neg inf mask
            if key == "jets":
                self.data["jets"][~self.jet_mask] = -T.inf

        ## If the data has been scaled and shifted yet
        self.is_processed = False

    def plot_variables(self, path):
        """Plot some histograms showing the input, context, and target
        distributions
        """

        ## Ensure the path exists
        path.mkdir(parents=True, exist_ok=True)

        ## Add a flag if the data has been procesed
        nrm_flag = "_prcd" if self.is_processed else ""

        ## For all data in the dictionary
        for key, data in self.data.items():
            plot_multi_hists(
                Path(path, key + nrm_flag),  ## Jet data is masked
                [data] if key != "jets" else [data[self.jet_mask]],
                [key],
                self.variables[key],
            )

    def save_preprocess_info(self, path: str) -> dict:
        """Return and save a dict of sklearn scalers which can preprocess the data"""
        print(" - calculating and saving preprocessing scalers")

        scalers = {}
        for key, data in self.data.items():

            scalers[key] = get_scaler(self.scaler_nm)

            ## Jet data must be masked
            if key == "jets":
                scalers[key].fit(data[self.jet_mask])  ## Jet data is masked
            else:
                scalers[key].fit(data)

        ## Save the scalers
        dump(scalers, path)

        return scalers

    def apply_preprocess(self, scalers: dict) -> None:
        """Apply the pre-processing steps to the stored data
         - Will break if the required named scalers are not present in the dictionary

        args:
            stats: dict containing named tensors describing how to prepare the data
        """
        print(" - applying preprocessing scalers")
        for key, data in self.data.items():
            ## Jet data must be masked
            if key == "jets":
                self.data[key][self.jet_mask] = T.from_numpy(
                    scalers[key].transform(data[self.jet_mask])  ## mask the jets
                ).float()
            else:
                self.data[key] = T.from_numpy(scalers[key].transform(data)).float()

        ## Make sure that jet data still adheres to the mask after transform
        self.data["jets"][~self.jet_mask] = -T.inf

        ## Change the processed state and change the names of the columns
        self.is_processed = True
        for key, names in self.variables.items():
            self.variables[key] = [nm + "_prcd" for nm in names]

    def __len__(self):
        return len(self.data["MET"])

    def __getitem__(self, idx):
        return [data[idx] for _, data in self.data.items()]

    def get_dim(self):
        """Return the dimensions of the sample returned by the getitem functon"""
        return [s.shape[-1] for s in self[0]]
